msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../<generated>:1
msgid "About KDE"
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:1
msgid "The help menu in Krita."
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "About"
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "Handbook"
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "Bug"
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:16
msgid "Help Menu"
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:18
msgid "Krita Handbook"
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:19
msgid "Opens a browser and sends you to the index of this manual."
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:20
msgid "Report Bug"
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:21
msgid "Sends you to the bugtracker."
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:22
msgid "Show system information for bug reports."
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:23
msgid ""
"This is a selection of all the difficult to figure out technical information "
"of your computer. This includes things like, which version of Krita you "
"have, which version your operating system is, and most prudently, what kind "
"of OpenGL functionality your computer is able to provide. The latter varies "
"a lot between computers and due that it is one of the most difficult things "
"to debug. Providing such information can help us figure out what is causing "
"a bug."
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:24
msgid "About Krita"
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:25
msgid "Shows you the credits."
msgstr ""

#: ../../reference_manual/main_menu/help_menu.rst:27
msgid "Tells you about the KDE community that Krita is part of."
msgstr ""
