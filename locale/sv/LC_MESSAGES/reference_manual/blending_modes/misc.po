# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:06+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/blending_modes/misc.rst:1
msgid ""
"Page about the miscellaneous blending modes in Krita: Bumpmap, Combine "
"Normal Map, Copy Red, Copy Green, Copy Blue, Copy and Dissolve."
msgstr ""
"Sida om diverse blandningslägen i Krita: Bulavbildning, Kombinera "
"normalavbildning, Kopiera Röd, Kopera Grön, Kopiera Blå, Kopiera och Lös upp."

#: ../../reference_manual/blending_modes/misc.rst:15
msgid "Misc"
msgstr "Diverse"

#: ../../reference_manual/blending_modes/misc.rst:17
msgid "Bumpmap (Blending Mode)"
msgstr "Bulavbildning (Blandningsläge)"

#: ../../reference_manual/blending_modes/misc.rst:21
msgid "Bumpmap"
msgstr "Bulavbildning"

#: ../../reference_manual/blending_modes/misc.rst:23
msgid "This filter seems to both multiply and respect the alpha of the input."
msgstr "Filtret verkar både multiplicera och respektera inmatningens alfa."

#: ../../reference_manual/blending_modes/misc.rst:25
#: ../../reference_manual/blending_modes/misc.rst:30
msgid "Combine Normal Map"
msgstr "Kombinera normalavbildning"

#: ../../reference_manual/blending_modes/misc.rst:25
msgid "Normal Map"
msgstr "Normal avbildning"

#: ../../reference_manual/blending_modes/misc.rst:32
msgid ""
"Mathematically robust blending mode for normal maps, using `Reoriented "
"Normal Map Blending <https://blog.selfshadow.com/publications/blending-in-"
"detail/>`_."
msgstr ""
"Matematiskt robust blandningsläge för normalavbildningar, med användning av "
"`Omorienterad normalavbildningsblandning <https://blog.selfshadow.com/"
"publications/blending-in-detail/>`_."

#: ../../reference_manual/blending_modes/misc.rst:34
msgid "Copy (Blending Mode)"
msgstr "Kopiera (Blandningsläge)"

#: ../../reference_manual/blending_modes/misc.rst:38
msgid "Copy"
msgstr "Kopiera"

#: ../../reference_manual/blending_modes/misc.rst:40
msgid ""
"Copies the previous layer exactly. Useful for when using filters and filter-"
"masks."
msgstr ""
"Kopierar föregående lager exakt. Användbart när filter och filtermasker "
"används."

#: ../../reference_manual/blending_modes/misc.rst:46
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:46
msgid "Left: **Normal**. Right: **Copy**."
msgstr "Vänster: **Normal**. Höger: **Kopiera**."

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Red"
msgstr "Kopiera röd"

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Green"
msgstr "Kopiera grön"

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Blue"
msgstr "Kopiera blå"

#: ../../reference_manual/blending_modes/misc.rst:54
msgid "Copy Red, Green, Blue"
msgstr "Kopiera röd, grön, blå"

#: ../../reference_manual/blending_modes/misc.rst:56
msgid ""
"This is a blending mode that will just copy/blend a source channel to a "
"destination channel. Specifically, it will take the specific channel from "
"the upper layer and copy that over to the lower layers."
msgstr ""
"Ett blandningsläge som bara kopierar eller blandar en källkanal till en "
"målkanal. Mer exakt, tar den specifika kanalen från det övre lagret och "
"kopierar över den till de undre lagren."

#: ../../reference_manual/blending_modes/misc.rst:59
msgid ""
"So, if you want the brush to only affect the red channel, set the blending "
"mode to 'copy red'."
msgstr ""
"Om man vill att penseln bara ska påverka den röda kanalen, ställ in "
"blandningsläget till 'kopiera röd'."

#: ../../reference_manual/blending_modes/misc.rst:65
msgid ""
".. image:: images/blending_modes/misc/Krita_Filter_layer_invert_greenchannel."
"png"
msgstr ""
".. image:: images/blending_modes/misc/Krita_Filter_layer_invert_greenchannel."
"png"

#: ../../reference_manual/blending_modes/misc.rst:65
msgid "The copy red, green and blue blending modes also work on filter-layers."
msgstr ""
"Blandningslägena för att kopiera röd, grön, och blå fungerar också för "
"filterlager."

#: ../../reference_manual/blending_modes/misc.rst:67
msgid ""
"This can also be done with filter layers. So if you quickly want to flip a "
"layer's green channel, make an invert filter layer with 'copy green' above "
"it."
msgstr ""
"Det kan också göras med filterlager. Om man alltså snabbt vill byta den "
"gröna kanalen på ett lager, ska man skapa ett inverterat filterlager med "
"'kopiera grön' ovanför det."

#: ../../reference_manual/blending_modes/misc.rst:72
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Red_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Red_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:72
msgid "Left: **Normal**. Right: **Copy Red**."
msgstr "Vänster: **Normal**. Höger: **Kopiera Röd**."

#: ../../reference_manual/blending_modes/misc.rst:78
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Green_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Green_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:78
msgid "Left: **Normal**. Right: **Copy Green**."
msgstr "Vänster: **Normal**. Höger: **Kopiera Grön**."

#: ../../reference_manual/blending_modes/misc.rst:84
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Blue_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Blue_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:84
msgid "Left: **Normal**. Right: **Copy Blue**."
msgstr "Vänster: **Normal**. Höger: **Kopiera Blå**."

#: ../../reference_manual/blending_modes/misc.rst:86
#: ../../reference_manual/blending_modes/misc.rst:90
msgid "Dissolve"
msgstr "Lös upp"

#: ../../reference_manual/blending_modes/misc.rst:92
msgid ""
"Instead of using transparency, this blending mode will use a random "
"dithering pattern to make the transparent areas look sort of transparent."
msgstr ""
"Istället för att använda genomskinlighet, använder blandningsläget ett "
"slumpmässigt gittermönster för att få genomskinliga områden att se ungefär "
"genomskinliga ut."

#: ../../reference_manual/blending_modes/misc.rst:97
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Dissolve_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Dissolve_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:97
msgid "Left: **Normal**. Right: **Dissolve**."
msgstr "Vänster: **Normal**. Höger: **Lös upp**."
