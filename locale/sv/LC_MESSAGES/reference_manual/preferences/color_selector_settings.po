# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:49+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Make Brush Color Bluer"
msgstr "Gör penselfärgen blåare"

#: ../../reference_manual/preferences/color_selector_settings.rst:1
msgid "The color selector settings in Krita."
msgstr "Inställningar av färgväljare i Krita."

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Preferences"
msgstr "Anpassning"

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Settings"
msgstr "Inställningar"

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Color Selector"
msgstr "Färgväljare"

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Color"
msgstr "Färg"

#: ../../reference_manual/preferences/color_selector_settings.rst:16
msgid "Color Selector Settings"
msgstr "Inställningar av färgväljare"

#: ../../reference_manual/preferences/color_selector_settings.rst:18
msgid ""
"These settings directly affect Advanced Color Selector Dockers and the same "
"dialog box appears when the user clicks the settings button in that docker "
"as well. They also affect certain hotkey actions."
msgstr ""
"Inställningarna påverkar direkt panelerna med Avancerad färgväljare och "
"samma dialogruta visas också när användaren klickar på inställningsknappen i "
"panelen. De påverkar också vissa genvägsåtgärder."

#: ../../reference_manual/preferences/color_selector_settings.rst:20
msgid ""
"This settings menu has a drop-down for Advanced Color Selector, and Color "
"Hotkeys."
msgstr ""
"Inställningsmenyn har en kombinationsruta för Avancerad färgväljare och "
"Färggenvägar."

#: ../../reference_manual/preferences/color_selector_settings.rst:23
msgid "Advanced Color Selector"
msgstr "Avancerad färgväljare"

#: ../../reference_manual/preferences/color_selector_settings.rst:25
msgid ""
"These settings are described on the page for the :ref:"
"`advanced_color_selector_docker`."
msgstr ""
"Inställningarna beskrivs på sidan för :ref:`advanced_color_selector_docker`."

#: ../../reference_manual/preferences/color_selector_settings.rst:28
msgid "Color Hotkeys"
msgstr "Färggenvägar"

#: ../../reference_manual/preferences/color_selector_settings.rst:30
msgid "These allow you to set the steps for the following actions:"
msgstr "Gör det möjligt att ställa in stegen för följande åtgärder:"

#: ../../reference_manual/preferences/color_selector_settings.rst:32
msgid "Make Brush Color Darker"
msgstr "Gör penselfärgen mörkare"

#: ../../reference_manual/preferences/color_selector_settings.rst:33
msgid ""
"This is defaultly set to :kbd:`K` key and uses the :guilabel:`lightness` "
"steps. This uses luminance when possible."
msgstr ""
"Det är normalt inställt till tangenten :kbd:`K` och använder stegen :"
"guilabel:`ljushet`. Använder luminans om möjligt."

#: ../../reference_manual/preferences/color_selector_settings.rst:34
msgid "Make Brush Color Lighter"
msgstr "Gör penselfärgen ljusare"

#: ../../reference_manual/preferences/color_selector_settings.rst:35
msgid ""
"This is defaultly set to :kbd:`L` key and uses the :guilabel:`lightness` "
"steps. This uses luminance when possible."
msgstr ""
"Det är normalt inställt till tangenten :kbd:`L` och använder stegen :"
"guilabel:`ljushet`. Använder luminans om möjligt."

#: ../../reference_manual/preferences/color_selector_settings.rst:36
msgid "Make Brush Color More Saturated"
msgstr "Gör penselfärgen mer mättad"

#: ../../reference_manual/preferences/color_selector_settings.rst:37
#: ../../reference_manual/preferences/color_selector_settings.rst:39
msgid "This is defaultly unset and uses the :guilabel:`saturation` steps."
msgstr ""
"Det är normalt inte inställt och använder steget :guilabel:`färgmättnad`."

#: ../../reference_manual/preferences/color_selector_settings.rst:38
msgid "Make Brush Color More Desaturated"
msgstr "Gör penselfärgen mindre mättad"

#: ../../reference_manual/preferences/color_selector_settings.rst:40
msgid "Shift Brushcolor Hue clockwise"
msgstr "Skifta pensels färgton medurs"

#: ../../reference_manual/preferences/color_selector_settings.rst:41
#: ../../reference_manual/preferences/color_selector_settings.rst:43
msgid "This is defaultly unset and uses the :guilabel:`Hue` steps."
msgstr "Det är normalt inte inställt och använder steget :guilabel:`Färgton`."

#: ../../reference_manual/preferences/color_selector_settings.rst:42
msgid "Shift Brushcolor Hue counter-clockwise"
msgstr "Skifta pensels färgton moturs"

#: ../../reference_manual/preferences/color_selector_settings.rst:44
msgid "Make Brush Color Redder"
msgstr "Gör penselfärgen rödare"

#: ../../reference_manual/preferences/color_selector_settings.rst:45
#: ../../reference_manual/preferences/color_selector_settings.rst:47
msgid "This is defaultly unset and uses the :guilabel:`Redder/Greener` steps."
msgstr ""
"Det är normalt inte inställt och använder stegen :guilabel:`Rödare/Grönare`."

#: ../../reference_manual/preferences/color_selector_settings.rst:46
msgid "Make Brush Color Greener"
msgstr "Gör penselfärgen grönare"

#: ../../reference_manual/preferences/color_selector_settings.rst:48
msgid "Make Brush Color Yellower"
msgstr "Gör penselfärgen gulare"

#: ../../reference_manual/preferences/color_selector_settings.rst:49
#: ../../reference_manual/preferences/color_selector_settings.rst:51
msgid "This is defaultly unset and uses the :guilabel:`Bluer/Yellower` steps."
msgstr ""
"Det är normalt inte inställt och använder stegen :guilabel:`Blåare/Gulare`."

#~ msgid ""
#~ "This is defaultly unset and uses the :guilabel:`Redder/Greener`  steps."
#~ msgstr ""
#~ "Det är normalt inte inställt och använder stegen :guilabel:`Rödare/"
#~ "Grönare`."
