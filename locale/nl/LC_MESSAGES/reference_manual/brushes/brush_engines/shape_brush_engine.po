# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-30 16:18+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Hard Edge"
msgstr "Harde rand"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:1
msgid "The Shape Brush Engine manual page."
msgstr "De handleidingpagina van Vormpenseel-engine."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:16
msgid "Shape Brush Engine"
msgstr "Vormpenseel-engine"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Penseel-engine"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
msgid "Experiment Brush Engine"
msgstr "Experimentpenseel-engine"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
msgid "Al.Chemy"
msgstr "Al.Chemy"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:19
msgid ".. image:: images/icons/shapebrush.svg"
msgstr ".. image:: images/icons/shapebrush.svg"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:20
msgid "An Al.chemy inspired brush-engine. Good for making chaos with!"
msgstr ""
"Een op Al.chemy geïnspireerde penseel-engine. Goed om er chaos mee te maken!"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:23
msgid "Parameters"
msgstr "Parameters"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:25
msgid ":ref:`option_experiment`"
msgstr ":ref:`option_experiment`"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:26
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:31
msgid "Experiment Option"
msgstr "Optie voor experiment"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:33
msgid "Speed"
msgstr "Snelheid"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:34
msgid ""
"This makes the outputted contour jaggy. The higher the speed, the jaggier."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:35
msgid "Smooth"
msgstr "Glad"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:36
msgid ""
"Smoothens the output contour. This slows down the brush, but the higher the "
"smooth, the smoother the contour."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:37
msgid "Displace"
msgstr "Verplaatsen"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:38
msgid ""
"This displaces the shape. The slow the movement, the higher the displacement "
"and expansion. Fast movements shrink the shape."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:39
msgid "Winding Fill"
msgstr "Winding vullen"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:40
msgid ""
"This gives you the option to use a 'non-zero' fill rules instead of the "
"'even-odd' fill rule, which means that where normally crossing into the "
"shape created transparent areas, it now will not."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:42
msgid "Removes the anti-aliasing, to get a pixelized line."
msgstr ""
