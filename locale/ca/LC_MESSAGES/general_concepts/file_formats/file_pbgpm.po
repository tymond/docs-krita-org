# Translation of docs_krita_org_general_concepts___file_formats___file_pbgpm.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 13:43+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../<generated>:1
msgid ".ppm"
msgstr ".ppm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:1
msgid "The PBM, PGM and PPM file formats as exported by Krita."
msgstr "Els formats de fitxer PBM, PGM i PPM tal com els exporta el Krita."

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "*.pbm"
msgstr "*.pbm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "*.pgm"
msgstr "*.pgm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "*.ppm"
msgstr "*.ppm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PBM"
msgstr "PBM"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PGM"
msgstr "PGM"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PPM"
msgstr "PPM"

#: ../../general_concepts/file_formats/file_pbgpm.rst:17
msgid "\\*.pbm, \\*.pgm and \\*.ppm"
msgstr "\\*.pbm, \\*.pgm i \\*.ppm"

# skip-rule: t-acc_obe
#: ../../general_concepts/file_formats/file_pbgpm.rst:18
msgid ""
"``.pbm``, ``.pgm`` and ``.ppm`` are a series of file-formats with a similar "
"logic to them. They are designed to save images in a way that the result can "
"be read as an ASCII file, from back when email clients couldn't read images "
"reliably."
msgstr ""
"Els ``.pbm``, ``.pgm`` i ``.ppm`` són una sèrie de formats de fitxer amb una "
"lògica similar. Estan dissenyats per a desar imatges de manera que el "
"resultat es pugui llegir com a fitxer ASCII -de quan els clients de correu "
"electrònic no podien llegir les imatges de manera fiable-."

#: ../../general_concepts/file_formats/file_pbgpm.rst:20
msgid ""
"They are very old file formats, and not used outside of very specialized "
"usecases, such as embedding images inside code."
msgstr ""
"Són formats de fitxer molt antics i no s'utilitzen fora dels casos d'ús molt "
"especials, com ara incrustar imatges dins del codi."

#: ../../general_concepts/file_formats/file_pbgpm.rst:22
msgid ".pbm"
msgstr ".pbm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:23
msgid "One-bit and can only show strict black and white."
msgstr "D'un bit i només pot mostrar-se en estricte blanc i negre."

#: ../../general_concepts/file_formats/file_pbgpm.rst:24
msgid ".pgm"
msgstr ".pgm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:25
msgid "Can show 255 values of gray (8bit)."
msgstr "Pot mostrar 255 valors de gris (de 8 bits)."

#: ../../general_concepts/file_formats/file_pbgpm.rst:27
msgid "Can show 8bit rgb values."
msgstr "Pot mostrar valors RGB de 8 bits."
