# Translation of docs_krita_org_reference_manual___tools___path_select.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-24 16:15+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<generated>:1
msgid "Activate Angle Snap"
msgstr "Activa l'ajust de l'angle"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: clic dret del ratolí"

#: ../../<rst_epilog>:72
msgid ""
".. image:: images/icons/path_select_tool.svg\n"
"   :alt: toolselectpath"
msgstr ""
".. image:: images/icons/path_select_tool.svg\n"
"   :alt: eina de selecció de camins"

#: ../../reference_manual/tools/path_select.rst:1
msgid "Krita's bezier curve selection tool reference."
msgstr "Referència de l'eina Corbes de Bézier del Krita."

#: ../../reference_manual/tools/path_select.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Vector"
msgstr "Vectors"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Path"
msgstr "Camí"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Bezier Curve"
msgstr "Corbes de Bézier"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Pen"
msgstr "Llapis"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Selection"
msgstr "Selecció"

#: ../../reference_manual/tools/path_select.rst:17
msgid "Path Selection Tool"
msgstr "Eina de selecció del camí"

#: ../../reference_manual/tools/path_select.rst:19
msgid "|toolselectpath|"
msgstr "|toolselectpath|"

#: ../../reference_manual/tools/path_select.rst:21
msgid ""
"This tool, represented by an ellipse with a dashed border and a curve "
"control, allows you to make a :ref:`selections_basics` of an area by drawing "
"a path around it. Click where you want each point of the path to be. Click "
"and drag to curve the line between points. Finally click on the first point "
"you created to close your path."
msgstr ""
"Aquesta eina, representada per una el·lipse amb una vora discontinua i un "
"control de la corba, permet crear una :ref:`selections_basics` d'una àrea "
"dibuixant un camí al seu voltant. Feu clic on vulgueu que resti cada punt "
"del camí. Feu clic i arrossegueu entre els punts per a corbar la línia. "
"Finalment, feu clic al primer punt que heu creat per a tancar el camí."

#: ../../reference_manual/tools/path_select.rst:24
msgid "Hotkeys and Sticky keys"
msgstr "Dreceres i tecles apegaloses"

#: ../../reference_manual/tools/path_select.rst:26
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""
":kbd:`R` estableix la selecció a «Substitueix» a les Opcions de l'eina, és "
"el mode predeterminat."

#: ../../reference_manual/tools/path_select.rst:27
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ":kbd:`A` estableix la selecció a «Afegeix» a les Opcions de l'eina."

#: ../../reference_manual/tools/path_select.rst:28
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ":kbd:`S` estableix la selecció a «Sostreu» a les Opcions de l'eina."

#: ../../reference_manual/tools/path_select.rst:29
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""
":kbd:`Majús. + feu` |mouseleft| estableix la selecció subsegüent a "
"«Afegeix». Podeu alliberar la tecla :kbd:`Majús.` mentre s'arrossega, però "
"encara serà establerta a «Afegeix». El mateix per a les altres."

#: ../../reference_manual/tools/path_select.rst:30
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""
":kbd:`Alt + feu` |mouseleft| estableix la selecció subsegüent a «Sostreu»."

#: ../../reference_manual/tools/path_select.rst:31
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""
":kbd:`Ctrl + feu` |mouseleft| estableix la selecció subsegüent a "
"«Substitueix»."

#: ../../reference_manual/tools/path_select.rst:32
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""
":kbd:`Majús. + Alt + feu` |mouseleft| estableix la selecció subsegüent a "
"«Interseca»."

#: ../../reference_manual/tools/path_select.rst:37
msgid "Hovering over a selection allows you to move it."
msgstr "Passar el cursor sobre una selecció permet moure-la."

#: ../../reference_manual/tools/path_select.rst:38
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""
"Fer |mouseright| obrirà un menú ràpid de selecció amb la possibilitat "
"d'editar la selecció, entre d'altres."

#: ../../reference_manual/tools/path_select.rst:43
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""
"Podeu canviar el comportament de la tecla :kbd:`Alt` per utilitzar la tecla :"
"kbd:`Ctrl` en lloc d'alternar el canvi als :ref:`general_settings`."

#: ../../reference_manual/tools/path_select.rst:46
msgid "Tool Options"
msgstr "Opcions de l'eina"

#: ../../reference_manual/tools/path_select.rst:50
#: ../../reference_manual/tools/path_select.rst:58
msgid "Autosmooth Curve"
msgstr "Corba de moviment suau"

#: ../../reference_manual/tools/path_select.rst:51
#: ../../reference_manual/tools/path_select.rst:59
msgid ""
"Toggling this will have nodes initialize with smooth curves instead of "
"angles. Untoggle this if you want to create sharp angles for a node. This "
"will not affect curve sharpness from dragging after clicking."
msgstr ""
"En alternar, els nodes s'inicialitzaran amb corbes suaus en lloc d'angles. "
"Desactiveu aquesta opció si voleu crear angles més definits per a un node. "
"Això no afectarà la nitidesa de la corba en arrossegar després de fer clic."

#: ../../reference_manual/tools/path_select.rst:54
msgid "Anti-aliasing"
msgstr "Antialiàsing"

#: ../../reference_manual/tools/path_select.rst:54
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Alterna entre donar o no seleccions amb vores suaus. Hi ha qui prefereix "
"vores precises per a les seves seleccions."

#: ../../reference_manual/tools/path_select.rst:61
msgid "Angle Snapping Delta"
msgstr "Delta de l'ajust de l'angle"

#: ../../reference_manual/tools/path_select.rst:62
msgid "The angle to snap to."
msgstr "L'angle al que s'ajustarà."

#: ../../reference_manual/tools/path_select.rst:64
msgid ""
"Angle snap will make it easier to have the next line be at a specific angle "
"of the current. The angle is determined by the :guilabel:`Angle Snapping "
"Delta`."
msgstr ""
"L'ajust de l'angle farà que sigui més fàcil tenir la següent línia en un "
"angle específic de l'actual. L'angle estarà determinat pel :guilabel:`Delta "
"de l'ajust de l'angle`."
