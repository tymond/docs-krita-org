# Translation of docs_krita_org_reference_manual___blending_modes___binary.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-06 03:34+0200\n"
"PO-Revision-Date: 2019-09-01 16:53+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../reference_manual/blending_modes/binary.rst:1
msgid "Page about the binary blending modes in Krita:"
msgstr "Pàgina sobre els modes de barreja binaris al Krita:"

#: ../../reference_manual/blending_modes/binary.rst:10
#: ../../reference_manual/blending_modes/binary.rst:14
msgid "Binary"
msgstr "Binari"

#: ../../reference_manual/blending_modes/binary.rst:16
msgid ""
"Binary modes are special class of blending modes which utilizes binary "
"operators for calculations. Binary modes are unlike every other blending "
"modes as these modes have a fractal attribute with falloff similar to other "
"blending modes. Binary modes can be used for generation of abstract art "
"using layers with very smooth surface. All binary modes have capitalized "
"letters to distinguish themselves from other blending modes."
msgstr ""
"Els modes binaris són una classe especial de modes de barreja que utilitzen "
"operadors binaris per als càlculs. Els modes binaris són diferents de tots "
"els altres modes de barreja, ja que aquests modes tenen un atribut fractal "
"amb un decaïment similar a altres modes de barreja. Els modes binaris es "
"poden utilitzar per a la generació d'art abstracte emprant capes amb una "
"superfície molt suau. Tots els modes binaris tenen lletres en majúscules per "
"a distingir-se d'altres modes de barreja."

#: ../../reference_manual/blending_modes/binary.rst:18
msgid ""
"To clarify on how binary modes works, convert decimal values to binary "
"values, then treat 1 or 0 as T or F respectively, and use binary operation "
"to get the end result, and then convert the result back to decimal."
msgstr ""
"Per aclarir com funcionen els modes binaris, convertiu els valors decimals "
"en valors binaris, després tracteu 1 o 0 com a T o F respectivament, i "
"utilitzeu l'operació binària per obtenir el resultat final, i després torneu "
"a convertir el resultat a decimal."

#: ../../reference_manual/blending_modes/binary.rst:22
msgid ""
"Binary blending modes do not work on float images or negative numbers! So, "
"don't report bugs about using binary modes on unsupported color space."
msgstr ""
"Els modes de barreja binaris no funcionen sobre imatges flotants o nombres "
"negatius! Per tant, no informeu d'errors sobre l'ús dels modes binaris sobre "
"espais de color no admesos."

#: ../../reference_manual/blending_modes/binary.rst:24
#: ../../reference_manual/blending_modes/binary.rst:28
msgid "AND"
msgstr "AND"

#: ../../reference_manual/blending_modes/binary.rst:30
msgid ""
"Performs the AND operation for the base and blend layer. Similar to multiply "
"blending mode."
msgstr ""
"Realitza l'operació AND per a les capes base i de la barreja. Similar al "
"mode de barreja Multiplica."

#: ../../reference_manual/blending_modes/binary.rst:35
msgid ".. image:: images/blending_modes/binary/Blend_modes_AND_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_AND_map.png"

#: ../../reference_manual/blending_modes/binary.rst:35
#: ../../reference_manual/blending_modes/binary.rst:40
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **AND**."
msgstr ""
"Esquerra: **Capa base**. Enmig: **Capa de la barreja**. Dreta: **AND**."

#: ../../reference_manual/blending_modes/binary.rst:40
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_AND_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_AND_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:41
#: ../../reference_manual/blending_modes/binary.rst:46
msgid "CONVERSE"
msgstr "CONVERSE"

#: ../../reference_manual/blending_modes/binary.rst:48
msgid ""
"Performs the inverse of IMPLICATION operation for the base and blend layer. "
"Similar to screen mode with blend layer and base layer inverted."
msgstr ""
"Realitza l'invers de l'operació IMPLICATION per a les capes base i de la "
"barreja. Similar al mode pantalla amb la capa de la barreja i la capa base "
"invertides."

#: ../../reference_manual/blending_modes/binary.rst:53
msgid ".. image:: images/blending_modes/binary/Blend_modes_CONVERSE_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_CONVERSE_map.png"

#: ../../reference_manual/blending_modes/binary.rst:53
#: ../../reference_manual/blending_modes/binary.rst:58
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **CONVERSE**."
msgstr ""
"Esquerra: **Capa base**. Enmig: **Capa de la barreja**. Dreta: **CONVERSE**."

#: ../../reference_manual/blending_modes/binary.rst:58
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_CONVERSE_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_CONVERSE_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:60
#: ../../reference_manual/blending_modes/binary.rst:64
msgid "IMPLICATION"
msgstr "IMPLICATION"

#: ../../reference_manual/blending_modes/binary.rst:66
msgid ""
"Performs the IMPLICATION operation for the base and blend layer. Similar to "
"screen mode with base layer inverted."
msgstr ""
"Realitza l'operació IMPLICATION per a les capes base i de la barreja. "
"Similar al mode pantalla amb la capa base invertida."

#: ../../reference_manual/blending_modes/binary.rst:71
msgid ".. image:: images/blending_modes/binary/Blend_modes_IMPLIES_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_IMPLIES_map.png"

#: ../../reference_manual/blending_modes/binary.rst:71
#: ../../reference_manual/blending_modes/binary.rst:76
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **IMPLICATION**."
msgstr ""
"Esquerra: **Capa base**. Enmig: **Capa de la barreja**. Dreta: "
"**IMPLICATION**."

#: ../../reference_manual/blending_modes/binary.rst:76
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_IMPLIES_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_IMPLIES_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:78
#: ../../reference_manual/blending_modes/binary.rst:82
msgid "NAND"
msgstr "NAND"

#: ../../reference_manual/blending_modes/binary.rst:84
msgid ""
"Performs the inverse of AND operation for base and blend layer. Similar to "
"the inverted multiply mode."
msgstr ""
"Realitza l'invers de l'operació AND per a les capes base i de la barreja. "
"Similar al mode multiplica invertit."

#: ../../reference_manual/blending_modes/binary.rst:89
msgid ".. image:: images/blending_modes/binary/Blend_modes_NAND_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_NAND_map.png"

#: ../../reference_manual/blending_modes/binary.rst:89
#: ../../reference_manual/blending_modes/binary.rst:94
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **NAND**."
msgstr ""
"Esquerra: **Capa base**. Enmig: **Capa de la barreja**. Dreta: **NAND**."

#: ../../reference_manual/blending_modes/binary.rst:94
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_NAND_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_NAND_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:96
#: ../../reference_manual/blending_modes/binary.rst:100
msgid "NOR"
msgstr "NOR"

#: ../../reference_manual/blending_modes/binary.rst:102
msgid ""
"Performs the inverse of OR operation for base and blend layer. Similar to "
"the inverted screen mode."
msgstr ""
"Realitza l'invers de l'operació OR per a les capes base i de la barreja. "
"Similar al mode pantalla invertit."

#: ../../reference_manual/blending_modes/binary.rst:107
msgid ".. image:: images/blending_modes/binary/Blend_modes_NOR_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_NOR_map.png"

#: ../../reference_manual/blending_modes/binary.rst:107
#: ../../reference_manual/blending_modes/binary.rst:112
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **NOR**."
msgstr ""
"Esquerra: **Capa base**. Enmig: **Capa de la barreja**. Dreta: **NOR**."

#: ../../reference_manual/blending_modes/binary.rst:112
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_NOR_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_NOR_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:114
#: ../../reference_manual/blending_modes/binary.rst:118
msgid "NOT CONVERSE"
msgstr "NOT CONVERSE"

#: ../../reference_manual/blending_modes/binary.rst:120
msgid ""
"Performs the inverse of CONVERSE operation for base and blend layer. Similar "
"to the multiply mode with base layer and blend layer inverted."
msgstr ""
"Realitza l'invers de l'operació CONVERSE per a les capes base i de la "
"barreja. Similar al mode multiplica amb la capa base i la capa de la barreja "
"invertides."

#: ../../reference_manual/blending_modes/binary.rst:125
msgid ""
".. image:: images/blending_modes/binary/Blend_modes_NOT_CONVERSE_map.png"
msgstr ""
".. image:: images/blending_modes/binary/Blend_modes_NOT_CONVERSE_map.png"

#: ../../reference_manual/blending_modes/binary.rst:125
#: ../../reference_manual/blending_modes/binary.rst:130
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **NOT CONVERSE**."
msgstr ""
"Esquerra: **Capa base**. Enmig: **Capa de la barreja**. Dreta: **NOT "
"CONVERSE**."

#: ../../reference_manual/blending_modes/binary.rst:130
msgid ""
".. image:: images/blending_modes/binary/"
"Blending_modes_NOT_CONVERSE_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/"
"Blending_modes_NOT_CONVERSE_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:132
#: ../../reference_manual/blending_modes/binary.rst:136
msgid "NOT IMPLICATION"
msgstr "NOT IMPLICATION"

#: ../../reference_manual/blending_modes/binary.rst:138
msgid ""
"Performs the inverse of IMPLICATION operation for base and blend layer. "
"Similar to the multiply mode with the blend layer inverted."
msgstr ""
"Realitza l'invers de l'operació IMPLICATION per a les capes base i de la "
"barreja. Similar al mode multiplica amb la capa de la barreja invertida."

#: ../../reference_manual/blending_modes/binary.rst:143
msgid ""
".. image:: images/blending_modes/binary/Blend_modes_NOT_IMPLICATION_map.png"
msgstr ""
".. image:: images/blending_modes/binary/Blend_modes_NOT_IMPLICATION_map.png"

#: ../../reference_manual/blending_modes/binary.rst:143
#: ../../reference_manual/blending_modes/binary.rst:148
msgid ""
"Left: **Base Layer**. Middle: **Blend Layer**. Right: **NOT IMPLICATION**."
msgstr ""
"Esquerra: **Capa base**. Enmig: **Capa de la barreja**. Dreta: **NOT "
"IMPLICATION**."

#: ../../reference_manual/blending_modes/binary.rst:148
msgid ""
".. image:: images/blending_modes/binary/"
"Blending_modes_NOT_IMPLICATION_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/"
"Blending_modes_NOT_IMPLICATION_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:150
#: ../../reference_manual/blending_modes/binary.rst:154
msgid "OR"
msgstr "OR"

#: ../../reference_manual/blending_modes/binary.rst:156
msgid ""
"Performs the OR operation for base and blend layer. Similar to screen mode."
msgstr ""
"Realitza l'operació OR per a les capes base i de la barreja. Similar al mode "
"pantalla."

#: ../../reference_manual/blending_modes/binary.rst:161
msgid ".. image:: images/blending_modes/binary/Blend_modes_OR_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_OR_map.png"

#: ../../reference_manual/blending_modes/binary.rst:161
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **OR**."
msgstr "Esquerra: **Capa base**. Enmig: **Capa de la barreja**. Dreta: **OR**."

#: ../../reference_manual/blending_modes/binary.rst:166
msgid ".. image:: images/blending_modes/binary/Blending_modes_OR_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_OR_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:166
#: ../../reference_manual/blending_modes/binary.rst:179
#: ../../reference_manual/blending_modes/binary.rst:184
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **XOR**."
msgstr ""
"Esquerra: **Capa base**. Enmig: **Capa de la barreja**. Dreta: **XOR**."

#: ../../reference_manual/blending_modes/binary.rst:168
#: ../../reference_manual/blending_modes/binary.rst:172
msgid "XOR"
msgstr "XOR"

#: ../../reference_manual/blending_modes/binary.rst:174
msgid ""
"Performs the XOR operation for base and blend layer. This mode has a special "
"property that if you duplicate the blend layer twice, you get the base layer."
msgstr ""
"Realitza l'operació XOR per a les capes base i de la barreja. Aquest mode té "
"una propietat especial que si dupliqueu la capa de la barreja dues vegades, "
"obtindreu la capa base."

#: ../../reference_manual/blending_modes/binary.rst:179
msgid ".. image:: images/blending_modes/binary/Blend_modes_XOR_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_XOR_map.png"

#: ../../reference_manual/blending_modes/binary.rst:184
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_XOR_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_XOR_Gradients.png"

#: ../../reference_manual/blending_modes/binary.rst:186
#: ../../reference_manual/blending_modes/binary.rst:190
msgid "XNOR"
msgstr "XNOR"

#: ../../reference_manual/blending_modes/binary.rst:192
msgid ""
"Performs the XNOR operation for base and blend layer. This mode has a "
"special property that if you duplicate the blend layer twice, you get the "
"base layer."
msgstr ""
"Realitza l'operació XNOR per a les capes base i de la barreja. Aquest mode "
"té una propietat especial que si dupliqueu la capa de la barreja dues "
"vegades, obtindreu la capa base."

#: ../../reference_manual/blending_modes/binary.rst:197
msgid ".. image:: images/blending_modes/binary/Blend_modes_XNOR_map.png"
msgstr ".. image:: images/blending_modes/binary/Blend_modes_XNOR_map.png"

#: ../../reference_manual/blending_modes/binary.rst:197
#: ../../reference_manual/blending_modes/binary.rst:202
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **XNOR**."
msgstr ""
"Esquerra: **Capa base**. Enmig: **Capa de la barreja**. Dreta: **XNOR**."

#: ../../reference_manual/blending_modes/binary.rst:202
msgid ""
".. image:: images/blending_modes/binary/Blending_modes_XNOR_Gradients.png"
msgstr ""
".. image:: images/blending_modes/binary/Blending_modes_XNOR_Gradients.png"
