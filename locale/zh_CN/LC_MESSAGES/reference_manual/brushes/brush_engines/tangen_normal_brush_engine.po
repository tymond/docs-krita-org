msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_engines___tangen_normal_brush_engine."
"pot\n"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_1.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_2.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutorial_3.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:0
msgid ".. image:: images/brushes/Krita-normals-tutoria_4.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:1
msgid "The Tangent Normal Brush Engine manual page."
msgstr "介绍 Krita 的切线空间法线笔刷引擎。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:16
msgid "Tangent Normal Brush Engine"
msgstr "切线空间法线笔刷引擎"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
msgid "Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:11
msgid "Normal Map"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:20
msgid ".. image:: images/icons/tangentnormal.svg"
msgstr ".. image:: images/icons/tangentnormal.svg"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:21
msgid ""
"The Tangent Normal Brush Engine is an engine that is specifically designed "
"for drawing normal maps, of the tangent variety. These are in turn used in "
"3d programs and game engines to do all sorts of lightning trickery. Common "
"uses of normal maps include faking detail where there is none, and to drive "
"transformations (Flow Maps)."
msgstr ""
"这是一个专门用于绘制在切线空间中的法线贴图的笔刷引擎。切线空间法线贴图用于在 "
"3D 程序和游戏引擎中制作各种光照效果。常见的法线贴图用法包括在没有凹凸细节的表"
"面上模仿凹凸效果，还有用来控制变形方向的流向图等。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:23
msgid ""
"A Normal map is an image that holds information for vectors. In particular, "
"they hold information for Normal Vectors, which is the information for how "
"the light bends on a surface. Because Normal Vectors are made up of 3 "
"coordinates, just like colors, we can store and see this information as "
"colors."
msgstr ""
"法线贴图是保存有法线矢量信息的图像，这种信息用于描述光在物体表面的扭曲情况。"
"由于法线矢量由三个坐标值组成，与颜色恰好一样，所以我们可以把法线矢量信息作为"
"颜色来保存和查看。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:25
msgid ""
"Normals can be seen similar to the stylus on your tablet. Therefore, we can "
"use the tilt-sensors that are available to some tablets to generate the "
"color of the normals, which can then be used by a 3d program to do lighting "
"effects."
msgstr ""
"如果你在数位板上垂直立起压感笔，那么压感笔就在数位板的法线上。因此，如果数位"
"板带有倾斜传感器，我们就可以直接使用它的倾斜数据来绘制法线贴图了。这也是这个"
"笔刷引擎的功能——将数位板的倾斜数据转换为法线数据的颜色，制作可供 3D 程序使用"
"的切线空间法线贴图。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:27
msgid "In short, you will be able to paint with surfaces instead of colors."
msgstr ""
"总而言之，这个笔刷引擎绘制的不是真正的颜色，而是不同的表面形状，用于在 3D 程"
"序中制作光照效果。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:29
msgid "The following options are available to the tangent normal brush engine:"
msgstr "可用笔刷选项："

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:31
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:32
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:33
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:34
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:35
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:36
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:37
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:38
msgid ":ref:`option_softness`"
msgstr ":ref:`option_softness`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:39
msgid ":ref:`option_sharpness`"
msgstr ":ref:`option_sharpness`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:40
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:41
msgid ":ref:`option_scatter`"
msgstr ":ref:`option_scatter`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:42
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:43
msgid ":ref:`option_texture`"
msgstr ":ref:`option_texture`"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:46
msgid "Specific Parameters to the Tangent Normal Brush Engine"
msgstr "切线空间法线笔刷引擎的特有选项"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:50
msgid "Tangent Tilt"
msgstr "切线倾斜"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:52
msgid ""
"These are the options that determine how the normals are calculated from "
"tablet input."
msgstr "这些选项控制如何通过数位板输入信息计算出法线数值。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:54
msgid "Tangent Encoding"
msgstr "切线编码"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:55
msgid ""
"This allows you to set what each color channel means. Different programs set "
"different coordinates to different channels, a common version is that the "
"green channel might need to be inverted (-Y), or that the green channel is "
"actually storing the x-value (+X)."
msgstr ""
"此页面设置每种颜色通道对应的坐标。不同的程序的颜色通道和坐标的对应情况不同。"
"常见的是绿通道要被反转 (-Y)，或者在绿通道中保存 X 坐标值 (+X)。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:56
msgid "Tilt Options"
msgstr "倾斜选项"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:57
msgid "Allows you to choose which sensor is used for the X and Y."
msgstr "选择 X 和 Y 使用的传感器。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:58
msgid "Tilt"
msgstr "倾斜"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:59
msgid "Uses Tilt for the X and Y."
msgstr "使用倾斜传感器确定 X 和 Y。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:60
msgid "Direction"
msgstr "方向"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:61
msgid ""
"Uses the drawing angle for the X and Y and Tilt-elevation for the Z, this "
"allows you to draw flowmaps easily."
msgstr ""
"使用笔迹走向传感器来确定 X 和 Y，使用倾斜仰角传感器来确定 Z，方便绘制流向图。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:62
msgid "Rotation"
msgstr "旋转"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:63
msgid ""
"Uses rotation for the X and Y, and tilt-elevation for the Z. Only available "
"for specialized Pens."
msgstr ""
"使用笔身旋转传感器确定 X 和 Y，使用倾斜仰角传感器确定 Z。只有某些特种压感笔才"
"支持此模式。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:65
msgid "Elevation Sensitivity"
msgstr "仰角敏感度"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:65
msgid ""
"Allows you to change the range of the normal that are outputted. At 0 it "
"will only paint the default normal, at 1 it will paint all the normals in a "
"full hemisphere."
msgstr ""
"更改输出法线的范围，数值为 0 时仅绘制默认法线，数值为 1 时将绘制一个完整半球"
"内的所有法线。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:68
msgid "Usage"
msgstr "使用方法"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:70
msgid ""
"The Tangent Normal Map Brush Engine is best used with the Tilt Cursor, which "
"can be set in :menuselection:`Settings --> Configure Krita --> General --> "
"Outline Shape --> Tilt Outline`."
msgstr ""
"切线空间法线笔刷最好配合倾斜光标使用。你可以在菜单栏的 :menuselection:`设置 "
"--> 配置 Krita --> 常规 --> 轮廓形状 --> 倾斜度轮廓` 处切换。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:73
msgid "Normal Map authoring workflow"
msgstr "法线贴图制作流程"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:75
msgid "Create an image with a background color of (128, 128, 255) blue/purple."
msgstr "创建一张背景色为 (R = 128, G = 128, B = 255) 的蓝紫色图像。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:80
msgid "Setting up a background with the default color."
msgstr "为新图像指定该默认背景色。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:82
msgid ""
"Set up group with a :guilabel:`Phong Bumpmap` filter mask. Use the :guilabel:"
"`Use Normal map` checkbox on the filter to make it use normals."
msgstr ""
"新建一个分组图层，在里面新建一个 :guilabel:`凹凸贴图和法线贴图` 滤镜图层或者"
"为分组创建一个“凹凸贴图和法线贴图”滤镜蒙版。在滤镜选项中勾选 :guilabel:`使用"
"法线贴图` 。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:87
msgid ""
"Creating a phong bump map filter layer, make sure to check 'Use Normal map'."
msgstr "创建一个“凹凸贴图和法线贴图”滤镜图层，在它的选项中勾选“使用法线贴图”。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:92
msgid ""
"These settings give a nice daylight-esque lighting setup, with light 1 being "
"the sun, light 3 being the light from the sky, and light 2 being the light "
"from the ground."
msgstr ""
"上图的设置可以给出一个类似日光的照明环境。光源 1 是太阳，光源 3 是天空，光源 "
"2 是地面。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:94
msgid ""
"Make a :guilabel:`Normalize` filter layer or mask to normalize the normal "
"map before feeding it into the Phong bumpmap filter for the best results."
msgstr ""
"在法线贴图图层上方新建一个 :guilabel:`标准化` 滤镜图层，或者给法线贴图图层创"
"建一个“标准化”滤镜蒙版，这样就可以在传递到“凹凸贴图和法线贴图”滤镜之前进行处"
"理，得到最佳效果。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:95
msgid "Then, paint on layers in the group to get direct feedback."
msgstr "接下来就可以在分组内部的法线贴图图层上进行绘制，并直接观察到效果了。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:100
msgid ""
"Paint on the layer beneath the filters with the tangent normal brush to have "
"them be converted in real time."
msgstr ""
"在两个滤镜图层下方的图层中使用切线空间法线笔刷进行绘制，可以实时观察到转换后"
"的效果。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:102
msgid ""
"Finally, when done, hide the Phong bumpmap filter layer (but keep the "
"Normalize filter layer!), and export the normal map for use in 3d programs."
msgstr ""
"绘制完毕后，隐藏“凹凸贴图和法线贴图”图层，但记得保持“标准化”图层的可见性。现"
"在你就可以把法线贴图导出为 3D 程序使用的格式了！"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:105
msgid "Drawing Direction Maps"
msgstr "绘制方向贴图"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:107
msgid ""
"Direction maps are made with the :guilabel:`Direction` option in the :"
"guilabel:`Tangent Tilt` options. These normal maps are used to distort "
"textures in a 3d program (to simulate for example, the flow of water) or to "
"create maps that indicate how hair and brushed metal is brushed. Krita can't "
"currently give feedback on how a given direction map will influence a "
"distortion or shader, but these maps are a little easier to read."
msgstr ""
"方向贴图可以通过 :guilabel:`切线倾斜` 中的 :guilabel:`方向` 选项进行绘制。这"
"种贴图用于在 3D 程序中扭曲纹理，可以模拟水流、指示发丝和拉丝金属的方向。"
"Krita 当前无法给出方向贴图对某种扭曲或者着色器的影响，但方向贴图比较容易看"
"懂。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:109
msgid ""
"Just set the :guilabel:`Tangent Tilt` option to :guilabel:`Direction`, and "
"draw. The direction your brush draws in will be the direction that is "
"encoded in the colors."
msgstr ""
"勾选 :guilabel:`切线倾斜` 中的 :guilabel:`方向` 选项，然后便可以开始绘制方向"
"贴图了。笔刷的绘制方向将会被编码为颜色。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:112
msgid "Only editing a single channel"
msgstr "仅编辑单个通道"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:114
msgid ""
"Sometimes you only want to edit a single channel. In that case set the "
"blending mode of the brush to :guilabel:`Copy <channel>`, with <channel> "
"replaced with red, green or blue. These are under the :guilabel:`Misc` "
"section of the blending modes."
msgstr ""
"如果需要仅修改单个通道的信息，可以使用笔刷混色模式中的 :guilabel:`复制 <通道"
"名>` ， <通道名> 可以是红、绿、蓝。它们被归类在混色模式的 :guilabel:`其他` 分"
"类中。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:116
msgid ""
"So, if you want the brush to only affect the red channel, set the blending "
"mode to :guilabel:`Copy Red`."
msgstr ""
"如果你希望笔刷操作只影响到红通道的信息，把混色模式设为 :guilabel:`复制红通道"
"` 。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:121
msgid ".. image:: images/brushes/Krita_Filter_layer_invert_greenchannel.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:121
msgid "The copy red, green and blue blending modes also work on filter-layers."
msgstr "复制红、绿、蓝通道这几个混色模式也可以在滤镜图层上使用。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:123
msgid ""
"This can also be done with filter layers. So if you quickly want to flip a "
"layer's green channel, make an invert filter layer with :guilabel:`Copy "
"Green` above it."
msgstr ""
"这个操作也可以用滤镜图层来实现。如果你想快速地对一个图层的绿通道进行反相，你"
"可以在它上面新建一个“反相”滤镜图层，然后把它设为 :guilabel:`复制绿通道` 。"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:126
msgid "Mixing Normal Maps"
msgstr "合并法线贴图"

#: ../../reference_manual/brushes/brush_engines/tangen_normal_brush_engine.rst:128
msgid ""
"For mixing two normal maps, Krita has the :guilabel:`Combine Normal Map` "
"blending mode under :guilabel:`Misc`."
msgstr ""
"要合并两张法线贴图，可以使用在 :guilabel:`其他` 分类中的 :guilabel:`合并法线"
"贴图` 混色模式。"
