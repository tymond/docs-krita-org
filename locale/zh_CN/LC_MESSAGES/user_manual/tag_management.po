msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___tag_management.pot\n"

#: ../../user_manual/tag_management.rst:1
msgid "Detailed steps on how to use the tags to organize resources in Krita."
msgstr "详细介绍如何在 Krita 里面通过标签来管理资源。"

#: ../../user_manual/tag_management.rst:11
msgid "Tags"
msgstr ""

#: ../../user_manual/tag_management.rst:16
msgid "Tag Management"
msgstr "标签管理"

#: ../../user_manual/tag_management.rst:18
msgid ""
"Tags are how you organize common types of resources. They can be used with "
"brushes, gradients, patterns, and even brush tips. You can select them from "
"a drop-down menu above the resources. Selecting a tag will filter all the "
"resources by that tag. Selecting the tag of :guilabel:`All` will show all "
"resources.  Krita comes installed with a few default tags. You can create "
"and edit your own as well. The tags are managed similarly across the "
"different types of resources."
msgstr ""
"Krita 通过标签系统来管理常见资源。你可以把标签指定给笔刷、渐变、图案，甚至笔"
"尖。在资源列表顶端的下拉菜单选取一个标签，资源列表就会过滤出该标签里面的全部"
"资源。选中 :guilabel:`All` 标签将列出全部可用资源。Krita 自带了一些默认的标"
"签，你也可以自行编辑或者创建新的标签。各种资源的标签管理系统都是类似的。"

#: ../../user_manual/tag_management.rst:20
msgid ""
"You can use tags together with the :ref:`Pop-up Palette <navigation>` for "
"increased productivity."
msgstr "你可以配合 :ref:`浮动画具板 <navigation>` 功能使用标签，提高工作效率。"

#: ../../user_manual/tag_management.rst:23
msgid ".. image:: images/resources/Tag_Management.jpeg"
msgstr ""

#: ../../user_manual/tag_management.rst:25
msgid ""
"You can select different brush tags in the pop-up palette. This can be a "
"quick way to access your favorite brushes."
msgstr ""
"你可以在浮动画具板中以标签为组加载笔刷预设，这样你就可以在画布上快速选用常用"
"的笔刷了。"

#: ../../user_manual/tag_management.rst:28
msgid "Adding a New Tag for a Brush"
msgstr "新建标签"

#: ../../user_manual/tag_management.rst:30
msgid ""
"By pressing the :guilabel:`+` next to the tag selection, you will get an "
"option to add a tag. Type in the name you want and press the :kbd:`Enter` "
"key. You will need to go back to the :guilabel:`All` tag to start assigning "
"brushes."
msgstr ""

#: ../../user_manual/tag_management.rst:33
msgid "Assigning an Existing Tag to a Brush"
msgstr "指定标签"

#: ../../user_manual/tag_management.rst:35
msgid ""
"Right-click on a brush in the Brush Presets Docker. You will get an option "
"to assign a tag to the brush."
msgstr ""
"在标签选单中重新选中 :guilabel:`All` 标签，列出全部可用笔刷。右键单击一个喜欢"
"的笔刷，在弹出的菜单中选择“指定给标签”即可把该笔刷指定给某个标签。"

#: ../../user_manual/tag_management.rst:38
msgid "Changing a Tag's Name"
msgstr "重命名标签"

#: ../../user_manual/tag_management.rst:40
msgid ""
"Select the existing tag that you want to have changed from the drop-down. "
"Press the :guilabel:`+` icon next to the tag. You will get an option to "
"rename it. Press the :kbd:`Enter` key to confirm. All the existing brushes "
"will remain in the newly named tag."
msgstr ""

#: ../../user_manual/tag_management.rst:43
msgid "Deleting a Tag"
msgstr "删除标签"

#: ../../user_manual/tag_management.rst:44
msgid ""
"Select the existing tag that you want to have removed from the drop-down. "
"Press the :guilabel:`+` icon next to the tag. You will get an option to "
"remove it."
msgstr ""
"在标签下拉菜单中选中你需要删除的标签，然后点击该菜单右侧的 :guilabel:`标签` "
"按钮，在弹出的菜单中点击“删除此标签”。"

#: ../../user_manual/tag_management.rst:47
msgid ""
"The default brushes that come with Krita cannot have their default tags "
"removed."
msgstr "你不能移除 Krita 自带笔刷的默认标签。"
