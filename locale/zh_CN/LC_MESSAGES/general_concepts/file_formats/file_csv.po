msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-08-16 17:05\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_general_concepts___file_formats___file_csv.pot\n"

#: ../../general_concepts/file_formats/file_csv.rst:1
msgid "The CSV file format as exported by Krita."
msgstr "由 Krita 导出的 CSV 文件格式。"

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "*.csv"
msgstr ""

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "CSV"
msgstr ""

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "Comma Separated Values"
msgstr ""

#: ../../general_concepts/file_formats/file_csv.rst:15
msgid "\\*.csv"
msgstr "\\*.csv"

#: ../../general_concepts/file_formats/file_csv.rst:17
msgid ""
"``.csv`` is the abbreviation for Comma Separated Values. It is an open, "
"plain text spreadsheet format. Since the CSV format is a plain text itself, "
"it is possible to use a spreadsheet program or even a text editor to edit "
"the ``*.csv`` file."
msgstr ""

#: ../../general_concepts/file_formats/file_csv.rst:19
msgid ""
"Krita supports the CSV version used by TVPaint, to transfer layered "
"animation between these two softwares and probably with others, like "
"Blender. This is not an image sequence format, so use the document loading "
"and saving functions in Krita instead of the :guilabel:`Import animation "
"frames` and :guilabel:`Render Animation` menu items."
msgstr ""

#: ../../general_concepts/file_formats/file_csv.rst:21
msgid ""
"The format consists of a text file with ``.csv`` extension, together with a "
"folder under the same name and a ``.frames`` extension. The CSV file and the "
"folder must be on the same path location. The text file contains the "
"parameters for the scene, like the field resolution and frame rate, and also "
"contains the exposure sheet for the layers. The folder contains :ref:"
"`file_png` picture files. Unlike image sequences, a key frame instance is "
"only a single file and the exposure sheet links it to one or more frames on "
"the timeline."
msgstr ""

#: ../../general_concepts/file_formats/file_csv.rst:26
msgid ".. image:: images/Csv_spreadsheet.png"
msgstr ""

#: ../../general_concepts/file_formats/file_csv.rst:26
msgid "A ``.csv`` file as a spreadsheet in :program:`LibreOffice Calc`."
msgstr ""

#: ../../general_concepts/file_formats/file_csv.rst:28
msgid ""
"Krita can both export and import this format. It is recommended to use 8bit "
"sRGB color space because that's the only color space for :program:`TVPaint`. "
"Layer groups and layer masks are also not supported."
msgstr ""
"Krita 可以导出和导入此格式。建议使用 8位 sRGB 色彩空间，因为 :program:"
"`TVPaint` 只支持这一种色彩空间。图层分组和图层蒙版也不被支持。"

#: ../../general_concepts/file_formats/file_csv.rst:30
msgid ""
"TVPaint can only export this format by itself. In :program:`TVPaint 11`, use "
"the :guilabel:`Export to...` option of the :guilabel:`File` menu, and on the "
"upcoming :guilabel:`Export footage` window, use the :guilabel:`Clip: Layers "
"structure` tab."
msgstr ""
"TVPaint 只能导出单独的该格式的文件。在 :program:`TVPaint 11` 中，打开 :"
"guilabel:`File` （文件）菜单中的 :guilabel:`Export to...` （导出到...）选"
"项 ，在接下来的 :guilabel:`Export footage` 窗口，选择 :guilabel:`Clip: "
"Layers structure` 选项卡。"

#: ../../general_concepts/file_formats/file_csv.rst:35
msgid ".. image:: images/Csv_tvp_csvexport.png"
msgstr ""

#: ../../general_concepts/file_formats/file_csv.rst:35
msgid "Exporting into ``.csv`` in TVPaint."
msgstr ""

#: ../../general_concepts/file_formats/file_csv.rst:37
msgid ""
"To import this format back into TVPaint there is a George language script "
"extension. See the \"Packs, Plugins, Third party\" section on the TVPaint "
"community forum for more details and also if you need support for other "
"softwares. Moho/Anime Studio and Blender also have plugins to import this "
"format."
msgstr ""
"要将此格式重新导入到 TVPaint 中，可使用一个以 George 语言编写的脚本扩展。请参"
"阅 TVPaint 社区论坛上的“Packs, Plugins, Third party”（包、插件以及第三方内"
"容）分区。Moho/Anime Studio 和 Blender 也有插件来导入这种格式。"

#: ../../general_concepts/file_formats/file_csv.rst:42
msgid ""
"`CSV import script for TVPaint <https://forum.tvpaint.com/viewtopic.php?"
"f=26&t=9759>`_."
msgstr ""

#: ../../general_concepts/file_formats/file_csv.rst:43
msgid ""
"`CSV import script for Moho/Anime Studio <https://forum.tvpaint.com/"
"viewtopic.php?f=26&t=10050>`_."
msgstr ""

#: ../../general_concepts/file_formats/file_csv.rst:44
msgid ""
"`CSV import script for Blender <https://developer.blender.org/T47462>`_."
msgstr ""
