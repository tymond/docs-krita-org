# translation of docs_krita_org_general_concepts___colors___color_space_size.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___colors___color_space_size\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-01 13:10+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../general_concepts/colors/color_space_size.rst:None
#, fuzzy
#| msgid ""
#| ".. image:: images/en/color_category/Basiccolormanagement_compare4spaces."
#| "png"
msgid ""
".. image:: images/color_category/Basiccolormanagement_compare4spaces.png"
msgstr ""
".. image:: images/en/color_category/Basiccolormanagement_compare4spaces.png"

#: ../../general_concepts/colors/color_space_size.rst:1
msgid "About Color Space Size"
msgstr "O veľkosti farebného priestoru"

#: ../../general_concepts/colors/color_space_size.rst:10
msgid "Color"
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:10
#, fuzzy
#| msgid "Color Space Size"
msgid "Color Spaces"
msgstr "Veľkosť farebného priestoru"

#: ../../general_concepts/colors/color_space_size.rst:15
msgid "Color Space Size"
msgstr "Veľkosť farebného priestoru"

#: ../../general_concepts/colors/color_space_size.rst:17
msgid ""
"Using Krita's color space browser, you can see that there are many different "
"space sizes."
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:25
msgid "How do these affect your image, and why would you use them?"
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:27
msgid "There are three primary reasons to use a large space:"
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:29
msgid ""
"Even though you can't see the colors, the computer program does understand "
"them and can do color maths with it."
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:30
msgid ""
"For exchanging between programs and devices: most CMYK profiles are a little "
"bigger than our default sRGB in places, while in other places, they are "
"smaller. To get the best conversion, having your image in a space that "
"encompasses both your screen profile as your printer profile."
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:31
msgid ""
"For archival purposes. In other words, maybe monitors of the future will "
"have larger amounts of colors they can show (spoiler: they already do), and "
"this allows you to be prepared for that."
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:33
msgid "Let's compare the following gradients in different spaces:"
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:37
#, fuzzy
#| msgid ""
#| ".. image:: images/en/color_category/Basiccolormanagement_compare4spaces."
#| "png"
msgid ""
".. image:: images/color_category/Basiccolormanagement_gradientsin4spaces_v2."
"jpg"
msgstr ""
".. image:: images/en/color_category/Basiccolormanagement_compare4spaces.png"

#: ../../general_concepts/colors/color_space_size.rst:40
#, fuzzy
#| msgid ""
#| ".. image:: images/en/color_category/Basiccolormanagement_compare4spaces."
#| "png"
msgid ""
".. image:: images/color_category/"
"Basiccolormanagement_gradientsin4spaces_nonmanaged.png"
msgstr ""
".. image:: images/en/color_category/Basiccolormanagement_compare4spaces.png"

#: ../../general_concepts/colors/color_space_size.rst:41
msgid ""
"On the left we have an artifact-ridden color managed jpeg file with an ACES "
"sRGBtrc v2 profile attached (or not, if not, then you can see the exact "
"different between the colors more clearly). This should give an "
"approximation of the actual colors. On the right, we have an sRGB png that "
"was converted in Krita from the base file."
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:43
msgid ""
"Each of the gradients is the gradient from the max of a given channel. As "
"you can see, the mid-tone of the ACES color space is much brighter than the "
"mid-tone of the RGB colorspace, and this is because the primaries are "
"further apart."
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:45
msgid ""
"What this means for us is that when we start mixing or applying filters, "
"Krita can output values higher than visible, but also generate more correct "
"mixes and gradients. In particular, when color correcting, the bigger space "
"can help with giving more precise information."
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:47
msgid ""
"If you have a display profile that uses a LUT, then you can use perceptual "
"to give an indication of how your image will look."
msgstr ""

#: ../../general_concepts/colors/color_space_size.rst:49
msgid ""
"Bigger spaces do have the downside they require more precision if you do not "
"want to see banding, so make sure to have at the least 16bit per channel "
"when choosing a bigger space."
msgstr ""
