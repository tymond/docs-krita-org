# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-21 13:21+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Group Krita XYZ LAB CMYK ICC ColorSetEntry profiles\n"
"X-POFile-SpellExtra: colorset kpl GRAY KPL\n"

#: ../../general_concepts/file_formats/file_kpl.rst:1
msgid "The Krita Palette file format."
msgstr "O formato de ficheiros de Paletas do Krita."

#: ../../general_concepts/file_formats/file_kpl.rst:10
msgid "*.kpl"
msgstr "*.kpl"

#: ../../general_concepts/file_formats/file_kpl.rst:10
msgid "KPL"
msgstr "KPL"

#: ../../general_concepts/file_formats/file_kpl.rst:10
msgid "Krita Palette"
msgstr "Paleta do Krita"

#: ../../general_concepts/file_formats/file_kpl.rst:15
msgid "\\*.kpl"
msgstr "\\*.kpl"

#: ../../general_concepts/file_formats/file_kpl.rst:17
msgid ""
"Since 4.0, Krita has a new palette file-format that can handle colors that "
"are wide gamut, RGB, CMYK, XYZ, GRAY, or LAB, and can be of any of the "
"available bitdepths, as well as groups. These are Krita Palettes, or ``*."
"kpl``."
msgstr ""
"Desde o 4.0, o Krita tem um novo formato de ficheiros de paletas que "
"consegue lidar com cores do tipo gamute amplo, RGB, CMYK, XYZ, GRAY ou LAB, "
"podendo ter qualquer uma das profundidades de cor disponíveis, bem como "
"grupos. Estas são as Paletas do Krita, ou ``*.kpl``."

#: ../../general_concepts/file_formats/file_kpl.rst:19
msgid ""
"``*.kpl`` files are ZIP files, with two XMLs and ICC profiles inside. The "
"colorset XML contains the swatches as ColorSetEntry and Groups as Group. The "
"profiles.XML contains a list of profiles, and the ICC profiles themselves "
"are embedded to ensure compatibility over different computers."
msgstr ""
"Os ficheiros ``*.kpl`` são ficheiros ZIP, com dois ficheiros XML e os perfis "
"ICC dentro deles. O XML do 'colorset' contém os elementos como instâncias de "
"ColorSetEntry e os grupos como Group. O profiles.XML contém uma lista de "
"perfis; por outro lado, os próprios perfis de ICC estão incorporados para "
"garantir a compatibilidade em vários computadores."
