# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 10:19+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Photoshop en MyPaint\n"
"X-POFile-SpellExtra: ConfigureToolbarsBrushesandStuffCustom guilabel image\n"
"X-POFile-SpellExtra: menuselection kbd Krita ToolbarsShown\n"
"X-POFile-SpellExtra: configuretoolbars ConfigureToolbarsKrita images\n"
"X-POFile-SpellExtra: reatribuir ref Blender alt Kritamouseright\n"
"X-POFile-IgnoreConsistency: Blender\n"
"X-POFile-SpellExtra: preferences mouseright icons\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../reference_manual/main_menu/settings_menu.rst:None
msgid ".. image:: images/preferences/Configure_Toolbars_Krita.png"
msgstr ".. image:: images/preferences/Configure_Toolbars_Krita.png"

#: ../../reference_manual/main_menu/settings_menu.rst:None
msgid ""
".. image:: images/preferences/Configure_Toolbars_Brushes_and_Stuff_Custom.png"
msgstr ""
".. image:: images/preferences/Configure_Toolbars_Brushes_and_Stuff_Custom.png"

#: ../../reference_manual/main_menu/settings_menu.rst:None
msgid ".. image:: images/preferences/Toolbars_Shown.png"
msgstr ".. image:: images/preferences/Toolbars_Shown.png"

#: ../../reference_manual/main_menu/settings_menu.rst:1
msgid "The settings menu in Krita."
msgstr "O menu de configuração no Krita."

#: ../../reference_manual/main_menu/settings_menu.rst:17
msgid "Setting Menu"
msgstr "O Menu Configuração"

#: ../../reference_manual/main_menu/settings_menu.rst:19
msgid ""
"The Settings Menu houses the configurable options in Krita and where you "
"determine most of the \"look and feel\" of the application."
msgstr ""
"O menu de Configuração aloja as opções configuráveis no Krita e é onde "
"define a maior parte da \"aparência e comportamento\" da aplicação."

#: ../../reference_manual/main_menu/settings_menu.rst:21
#: ../../reference_manual/main_menu/settings_menu.rst:34
msgid "Dockers"
msgstr "Áreas Acopláveis"

#: ../../reference_manual/main_menu/settings_menu.rst:24
#: ../../reference_manual/main_menu/settings_menu.rst:31
msgid "Show Dockers"
msgstr "Mostrar as Áreas Acopláveis"

#: ../../reference_manual/main_menu/settings_menu.rst:27
msgid ""
"Determines whether or not the dockers are visible.  This is a nice aid to "
"cleaning up the interface and removing unnecessary \"eye-ball clutter\" when "
"you are painting.  If you've got your brush and you know you're just going "
"to be painting for awhile why not flip the dockers off?  You'd be amazed "
"what a difference it makes while you're working.  However, if you know "
"you're swapping out tools or working with layer or any of the other myriad "
"things Krita lets you do then there's no point getting caught up in flipping "
"the docks on and off.  Use you time wisely!"
msgstr ""
"Define se as áreas acopláveis estão visíveis ou não. Isto é uma boa ajuda "
"para ajudar a arrumar a interface e a remover \"confusão a olhos vistos\" "
"enquanto estiver a pintar. Se já tem o seu pincel seleccionado e sabe que só "
"vai estar a pintar nos próximos tempos, porque não desligar as áreas "
"acopláveis? Ficará espantado com a diferença que isso fará enquanto estiver "
"a trabalhar. Contudo, se souber que vai estar a trocar de ferramentas ou "
"fazer diversas coisas, o Krita permite-lhe fazer isso, caso ache que não "
"vale a pena estar a ligar e a desligar as áreas acopláveis. Use o seu tempo "
"de forma sábia!"

#: ../../reference_manual/main_menu/settings_menu.rst:31
msgid ""
"This is a great candidate to add to the toolbar so you can just click the "
"dockers on and off and don't even have to open the menu to do it. See :ref:"
"`configure_toolbars` below for more."
msgstr ""
"Este é um grande candidato a adicionar à barra de ferramentas, por isso "
"poderá simplesmente activar ou desactivar as áreas acopláveis, sem que tenha "
"de abrir o menu para esse efeito. Veja mais informações abaixo em :ref:"
"`configure_toolbars`."

#: ../../reference_manual/main_menu/settings_menu.rst:36
msgid ""
"Krita subdivides the access of many of its features into functional panels "
"called Dockers. Dockers are small windows that can contain, for example, "
"things like the Layer Stack, Color Palette or Brush Presets. Think of them "
"as the painter's palette, or his water, or his brushkit."
msgstr ""
"O Krita subdivide o acesso a muitas das suas funcionalidades em painéis "
"funcionais, chamados de Áreas Acopláveis. Estas são pequenas janelas que "
"poderão conter, por exemplo, algumas coisas como a Pilha de Camadas, a "
"Paleta de Cores ou as Predefinições de Pincéis. Pense nelas como a paleta do "
"pintor, a sua água ou o seu estojo de pincéis."

#: ../../reference_manual/main_menu/settings_menu.rst:38
msgid ""
"Learning to use dockers effectively is a key concept to optimizing your time "
"using Krita."
msgstr ""
"Aprender a usar as áreas acopláveis de forma eficaz é um conceito-chave para "
"optimizar o seu tempo no uso do Krita."

#: ../../reference_manual/main_menu/settings_menu.rst:40
#: ../../reference_manual/main_menu/settings_menu.rst:43
msgid "Themes"
msgstr "Temas"

#: ../../reference_manual/main_menu/settings_menu.rst:40
msgid "Theme"
msgstr "Tema"

#: ../../reference_manual/main_menu/settings_menu.rst:40
msgid "Look and Feel"
msgstr "Aparência e Comportamento"

#: ../../reference_manual/main_menu/settings_menu.rst:45
msgid ""
"Krita provides a number of color-themed interfaces or \"looks\".  The "
"current set of themes are the following:"
msgstr ""
"O Krita oferece um conjunto de interfaces com temas de cores ou \"visuais\". "
"O conjunto actual de temas é o seguinte:"

#: ../../reference_manual/main_menu/settings_menu.rst:47
msgid "Dark (Default)"
msgstr "Escuro (Por Omissão)"

#: ../../reference_manual/main_menu/settings_menu.rst:48
msgid "Blender"
msgstr "Blender"

#: ../../reference_manual/main_menu/settings_menu.rst:49
msgid "Bright"
msgstr "Claro"

#: ../../reference_manual/main_menu/settings_menu.rst:50
msgid "Neutral"
msgstr "Neutro"

#: ../../reference_manual/main_menu/settings_menu.rst:52
msgid ""
"There is no easy way to create and share themes. The color themes are "
"defined in the :menuselection:`Share --> Color Schemes` folder where Krita "
"is downloaded."
msgstr ""
"Não existe uma forma simples de criar e partilhar temas. Os temas de cores "
"estão definidos na pasta :menuselection:`Partilhar --> Esquemas de Cores` "
"para onde foi transferido o Krita."

#: ../../reference_manual/main_menu/settings_menu.rst:55
msgid "Configure Shortcuts"
msgstr "Configurar os Atalhos"

#: ../../reference_manual/main_menu/settings_menu.rst:57
msgid ""
"Configuring shortcuts is another way to customize the application to fit "
"you.  Whether you are transitioning from another app, like Photoshop or "
"MyPaint, or you think your own shortcut keys make more sense for you then "
"Krita has got you covered.  You get to the shortcuts interface through :"
"menuselection:`Settings --> Configure Krita`  and by choosing the :"
"menuselection:`Keyboard Shortcuts`  tab."
msgstr ""
"A configuração de atalhos é outra forma de personalizar a aplicação a seu "
"gosto. Se está a transitar de outra aplicação, como o Photoshop ou o "
"MyPaint, ou se achar que as suas próprias combinações de teclas fazem mais "
"sentido para si, o Krita pode ajudá-lo. Poderá aceder à interface dos "
"atalhos através da opção :menuselection:`Configuração --> Configurar o "
"Krita`  e escolhendo a página :menuselection:`Atalhos do Teclado`."

#: ../../reference_manual/main_menu/settings_menu.rst:59
msgid ""
"To use, just type the :guilabel:`Action` into the Search box you want to "
"assign/reassign the shortcut for.  Suppose we wanted to assign the shortcut :"
"kbd:`Ctrl + G` to the :guilabel:`Action` of Group Layers so that every time "
"we pressed the :kbd:`Ctrl + G` shortcut a new Layer Group would be created.  "
"Use the following steps to do this:"
msgstr ""
"Para usar, basta escrever a :guilabel:`Acção` no campo de Pesquisa onde "
"deseja atribuir ou reatribuir o atalho. Suponha que deseja atribuir a "
"combinação de teclas :kbd:`Ctrl + G` à :guilabel:`Acção` das Camadas do "
"Grupo, para que sempre que carregar em :kbd:`Ctrl + G` seja criada uma nova "
"Camada de Grupo. Use os seguintes passos para o fazer:"

#: ../../reference_manual/main_menu/settings_menu.rst:61
msgid "Type \"Group Layer\"."
msgstr "Escreva \"Camada de Grupo\"."

#: ../../reference_manual/main_menu/settings_menu.rst:62
msgid "Click on Group Layer and a small inset box will open."
msgstr "Carregue na \"Camada de Grupo\" para abrir um pequeno campo."

#: ../../reference_manual/main_menu/settings_menu.rst:63
msgid "Click the Custom radio button."
msgstr "Carregue na opção exclusiva Personalizada."

#: ../../reference_manual/main_menu/settings_menu.rst:64
msgid "Click on the first button and type the :kbd:`Ctrl + G` shortcut."
msgstr ""
"Carregue no primeiro botão e pressione a combinação de teclas :kbd:`Ctrl + "
"G`."

#: ../../reference_manual/main_menu/settings_menu.rst:65
msgid "Click OK."
msgstr "Carregue em OK."

#: ../../reference_manual/main_menu/settings_menu.rst:67
msgid ""
"From this point on, whenever you press the :kbd:`Ctrl + G` shortcut you'll "
"get a new :guilabel:`Group Layer`."
msgstr ""
"A partir daqui, sempre carregar em :kbd:`Ctrl + G`, irá obter uma nova :"
"guilabel:`Camada de Grupo`."

#: ../../reference_manual/main_menu/settings_menu.rst:70
msgid ""
"Smart use of shortcuts can save you significant time and further streamline "
"your workflow."
msgstr ""
"Um uso inteligente dos atalhos pode ajudá-lo a poupar bastante tempo e a "
"agilizar o seu fluxo de trabalho."

#: ../../reference_manual/main_menu/settings_menu.rst:73
msgid "Manage Resources"
msgstr "Gerir os Recursos"

#: ../../reference_manual/main_menu/settings_menu.rst:75
msgid ""
"Manage the resources. You can read more about it :ref:`here "
"<resource_management>`."
msgstr ""
"Faz a gestão dos recursos. Poderá ler mais sobre o assunto :ref:`aqui "
"<resource_management>`."

#: ../../reference_manual/main_menu/settings_menu.rst:77
msgid "Language"
msgstr "Língua"

#: ../../reference_manual/main_menu/settings_menu.rst:81
msgid "Switch Application Language"
msgstr "Mudar a Língua da Aplicação"

#: ../../reference_manual/main_menu/settings_menu.rst:83
msgid "If you wish to use Krita in a different translation."
msgstr "Se desejar usar o Krita numa tradução diferente."

#: ../../reference_manual/main_menu/settings_menu.rst:85
msgid "Toolbar"
msgstr "Barra de Ferramentas"

#: ../../reference_manual/main_menu/settings_menu.rst:89
msgid "Configure Toolbars"
msgstr "Configurar as Barras de Ferramentas"

#: ../../reference_manual/main_menu/settings_menu.rst:91
msgid ""
"Krita allows you to highly customize the Toolbar interface.  You can add, "
"remove and change the order of nearly everything to fit your style of work.  "
"To get started, choose :menuselection:`Settings --> Configure Toolbars`."
msgstr ""
"O Krita permite-lhe personalizar em grande medida a interface da Barra de "
"Ferramentas. Poderá adicionar, remover e modificar a ordem de praticamente "
"tudo, para se ajustar ao seu estilo de trabalho. Para começar, escolha :"
"menuselection:`Configuração --> Configurar as Barras de Ferramentas`."

#: ../../reference_manual/main_menu/settings_menu.rst:96
msgid "The dialog is broken down into three main sections:"
msgstr "A janela está decomposta em três secções principais:"

#: ../../reference_manual/main_menu/settings_menu.rst:98
msgid "The Toolbar"
msgstr "A Barra de Ferramentas"

#: ../../reference_manual/main_menu/settings_menu.rst:99
msgid "Choose to either modify the \"File\" or \"Brushes and Stuff\" toolbars."
msgstr "Opte por modificar as barras de \"Ficheiro\" ou \"Pincéis e Outros\"."

#: ../../reference_manual/main_menu/settings_menu.rst:100
msgid "Available Actions:"
msgstr "Acções Disponíveis:"

#: ../../reference_manual/main_menu/settings_menu.rst:101
msgid "All the options that can be added to a toolbar."
msgstr "Todas as opções que podem ser adicionadas a uma barra de ferramentas."

#: ../../reference_manual/main_menu/settings_menu.rst:103
msgid "Current Actions:"
msgstr "Acções Actuais:"

#: ../../reference_manual/main_menu/settings_menu.rst:103
msgid "All the actions currently assigned and the order they are in."
msgstr "Todas as acções atribuídas de momento e a ordem pela qual aparecem."

#: ../../reference_manual/main_menu/settings_menu.rst:105
msgid ""
"Use the arrows between the *Available* and *Current* actions sections to "
"move items back and forth and up and down in the hierarchy.  This type of "
"inclusion/exclusion interface has been around on PCs for decades so we don't "
"need to go into great detail regarding its use.  What is important though is "
"selecting the correct Toolbar to work on.  The :guilabel:`File` Toolbar "
"allows you to add items between the :menuselection:`New` , :menuselection:"
"`Open`  and :menuselection:`Save`  buttons as well as to the right of the :"
"menuselection:`Save`  button.  The :guilabel:`Brushes and Stuff` Toolbar, "
"lets you modify anything from the Gradients button over to the right.  This "
"is probably where you'll do most of your editing."
msgstr ""
"Use as setas entre as acções *Disponíveis* e as *Actuais* para distribuir os "
"itens pelo seu estado e subindo ou descendo as mesmas na hierarquia. Este "
"tipo de interface de inclusão/exclusão tem existido nos PC's durante "
"demasiado tempo, pelo que não é preciso grande detalhe a explicar o seu uso. "
"O que é importante é a selecção da Barra de Ferramentas correcta. A Barra "
"de :guilabel:`Ficheiros` permite-lhe adicionar itens entre os botões :"
"menuselection:`Novo` , :menuselection:`Abrir` e :menuselection:`Gravar`, "
"assim como a seguir aos mesmos. A barra de :guilabel:`Pincéis e Outros` "
"permite-lhe modificar tudo no botão de Gradientes à direita. Aqui é onde "
"provavelmente fará a maior parte das suas edições."

#: ../../reference_manual/main_menu/settings_menu.rst:107
msgid ""
"Here we've added :menuselection:`Select Opaque` , :menuselection:`Local "
"Selection` ,  :menuselection:`Transparency Mask` , :guilabel:`Isolate "
"Layer` , :menuselection:`Show Assistant Previews` .  This is just an example "
"of a couple of options that are used frequently and might trim your "
"workflow.   This is what it looks like in the configuration tool:"
msgstr ""
"Aqui foram adicionadas as opções :menuselection:`Selecção Opaca` , :"
"menuselection:`Selecção Local` ,  :menuselection:`Máscara de "
"Transparência` , :guilabel:`Isolar a Camada` , :menuselection:`Mostrar as "
"Antevisões do Assistente` .  Isto é apenas um exemplo de um conjunto de "
"opções que são usadas com frequência e que podem reduzir o âmbito do seu "
"fluxo de trabalho. Isto é como aparece na ferramenta de configuração:"

#: ../../reference_manual/main_menu/settings_menu.rst:112
msgid ""
"You'll notice that some of the items are text only and some only icons.  "
"This is determined by whether the particular item has an associated icon in "
"Krita.  You can select anything from the *Available* section and move it to "
"the *Current* one and rearrange to fit your own workflow."
msgstr ""
"Irá reparar que alguns dos itens só têm texto e outros só têm ícones. Isto "
"acontece caso esse item em particular tenha um ícone associado no Krita. "
"Poderá seleccionar tudo na secção *Disponíveis* e mover para os *Actuais*, "
"reorganizando-os de forma que melhor se ajuste ao seu fluxo."

#: ../../reference_manual/main_menu/settings_menu.rst:114
msgid ""
"If you add so many that they won't all fit on your screen at once, you will "
"see a small chevron icon appear.  Click it and the toolbar expands to show "
"the remaining items."
msgstr ""
"Se adicionar demasiados, de forma que não caibam todos de uma vez, irá ver "
"um pequeno ícone com aspas. Carregue nele para expandir a área de "
"ferramentas e mostrar os itens em falta."

#: ../../reference_manual/main_menu/settings_menu.rst:117
msgid "Toolbars shown"
msgstr "Barras de ferramentas visíveis"

#: ../../reference_manual/main_menu/settings_menu.rst:117
msgid "Gives a list of toolbars that can be shown."
msgstr "Devolve uma lista das barras de ferramentas que podem estar visíveis."

#: ../../reference_manual/main_menu/settings_menu.rst:119
msgid ""
"At this time Krita does not support the ability to create additional "
"toolbars. The ones available are:"
msgstr ""
"Nesta altura, o Krita não suporta a capacidade de criar barras de "
"ferramentas adicionais. As que estão disponíveis são:"

#: ../../reference_manual/main_menu/settings_menu.rst:124
msgid ""
"Although not really advisable, you can turn them off (but why would you.."
"really?)"
msgstr ""
"Ainda que não seja de facto aconselhável, podê-las-á desligar (se bem que... "
"porquê?"

#: ../../reference_manual/main_menu/settings_menu.rst:126
msgid ""
"Finally, Toolbars also can be moved. You can do this by |mouseright| and "
"dragging the handler at the left side of the toolbar."
msgstr ""
"Finalmente, poderá também mover as barras de ferramentas. Poderá fazer isto "
"com o |mouseright| e arrastando a pega do lado esquerdo da barra de "
"ferramentas."
