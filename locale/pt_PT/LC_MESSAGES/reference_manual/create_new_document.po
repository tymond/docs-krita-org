# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:24+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Krita guilabel image menuselection kbd images kra ref\n"
"X-POFile-SpellExtra: generalconceptcolor Kritanewfile\n"

#: ../../reference_manual/create_new_document.rst:1
msgid ""
"A simple guide to the first basic steps of using Krita: creating and saving "
"an image."
msgstr ""
"Um guia simples para os primeiros passos básicos de utilização do Krita: a "
"criação e gravação de uma imagem."

#: ../../reference_manual/create_new_document.rst:18
msgid "Save"
msgstr "Gravar"

#: ../../reference_manual/create_new_document.rst:18
msgid "Load"
msgstr "Carregar"

#: ../../reference_manual/create_new_document.rst:18
msgid "New"
msgstr "Novo"

#: ../../reference_manual/create_new_document.rst:22
msgid "Create New Document"
msgstr "Criar um Novo Documento"

#: ../../reference_manual/create_new_document.rst:24
msgid "A new document can be created as follows."
msgstr "Pode ser criado um novo documento da seguinte forma."

#: ../../reference_manual/create_new_document.rst:26
msgid "Click on :guilabel:`File` from the application menu at the top."
msgstr "Carregue em :guilabel:`Ficheiro` no menu de aplicações no topo."

#: ../../reference_manual/create_new_document.rst:27
msgid ""
"Then click on :guilabel:`New`. Or you can do this by pressing the :kbd:`Ctrl "
"+ N` shortcut."
msgstr ""
"Depois carregue em :guilabel:`Novo`. Ou então poderá fazer isto se carregar "
"em :kbd:`Ctrl + N`."

#: ../../reference_manual/create_new_document.rst:28
msgid "Now you will get a New Document dialog box as shown below:"
msgstr "Agora irá obter uma janela de Novo Documento, como aparece abaixo:"

#: ../../reference_manual/create_new_document.rst:31
msgid ".. image:: images/Krita_newfile.png"
msgstr ".. image:: images/Krita_newfile.png"

#: ../../reference_manual/create_new_document.rst:32
msgid ""
"There are various sections in this dialog box which aid in creation of new "
"document, either using custom document properties or by using contents from "
"clipboard and templates. Following are the sections in this dialog box:"
msgstr ""
"Existem diversas secções nesta janela que ajudam na criação de um novo "
"documento, sejam através da utilização de propriedades de documentos "
"personalizadas ou através do uso do conteúdo da área de transferência ou de "
"modelos. Seguem-se as secções desta janela:"

#: ../../reference_manual/create_new_document.rst:37
msgid "Custom Document"
msgstr "Documento Personalizado"

#: ../../reference_manual/create_new_document.rst:39
msgid ""
"From this section you can create a document according to your requirements: "
"you can specify the dimensions, color model, bit depth, resolution, etc."
msgstr ""
"A partir desta secção, poderá criar um documento de acordo com os seus "
"requisitos: poderá indicar as dimensões, o modelo de cores, a profundidade "
"de bits, a resolução, etc."

#: ../../reference_manual/create_new_document.rst:42
msgid ""
"In the top-most field of the :guilabel:`Dimensions` tab, from the Predefined "
"drop-down you can select predefined pixel sizes and PPI (pixels per inch). "
"You can also set custom dimensions and the orientation of the document from "
"the input fields below the predefined drop-down. This can also be saved as a "
"new predefined preset for your future use by giving a name in the Save As "
"field and clicking on the Save button. Below we find the Color section of "
"the new document dialog box, where you can select the color model and the "
"bit-depth. Check :ref:`general_concept_color` for more detailed information "
"regarding color."
msgstr ""
"No campo de topo da área de :guilabel:`Dimensões`, na lista de "
"Predefinições, podem seleccionar os tamanhos de pixels e PPP (pontos por "
"polegada) predefinidos. Também poderá definir dimensões personalizadas e a "
"orientação do documento a partir dos campos de entrada da lista de "
"predefinições. Isto também pode ser gravado como uma nova predefinição para "
"seu uso futuro, atribuindo-lhe um nome no campo Gravar Como e depois "
"carregando no botão Gravar. Em baixo encontramos a secção da Cor da janela "
"de novo documento, onde poderá seleccionar o modelo de cores e a "
"profundidade de dados. Veja em :ref:`general_concept_color` mais informações "
"detalhadas em relação à cor."

#: ../../reference_manual/create_new_document.rst:52
msgid ""
"On the :guilabel:`Content` tab, you can define a name for your new document. "
"This name will appear in the metadata of the file, and Krita will use it for "
"the auto-save functionality as well. If you leave it empty, the document "
"will be referred to as 'Unnamed' by default. You can select the background "
"color and the amount of layers you want in the new document. Krita remembers "
"the amount of layers you picked last time, so be careful."
msgstr ""
"Na página de :guilabel:`Conteúdo`, poderá definir um nome para o seu novo "
"documento. Este nome irá aparecer nos meta-dados do ficheiro e o Krita usá-"
"lo-á também para a funcionalidade de gravação automática. Se o deixar em "
"branco, o documento ficará referenciado como 'Sem nome' por omissão. Poderá "
"seleccionar a cor de fundo e a quantidade de camadas que precisa no novo "
"documento. O Krita recorda a quantidade de camadas que escolheu da última "
"vez, por isso tenha cuidado."

#: ../../reference_manual/create_new_document.rst:59
msgid ""
"Finally, there's a description box, useful to note down what you are going "
"to do."
msgstr ""
"Finalmente, existe um campo de descrição, útil para indicar o que vai fazer."

#: ../../reference_manual/create_new_document.rst:62
msgid "Create From Clipboard"
msgstr "Criar a Partir da Área de Transferência"

#: ../../reference_manual/create_new_document.rst:64
msgid ""
"This section allows you to create a document from an image that is in your "
"clipboard, like a screenshot. It will have all the fields set to match the "
"clipboard image."
msgstr ""
"Esta secção permite-lhe criar um documento a partir de uma imagem que está "
"na sua área de transferência, como uma captura do ecrã. Ele irá ter todos os "
"campos configurados de forma a corresponder à imagem na área de "
"transferência."

#: ../../reference_manual/create_new_document.rst:69
msgid "Templates:"
msgstr "Modelos:"

#: ../../reference_manual/create_new_document.rst:71
msgid ""
"These are separate categories where we deliver special defaults. Templates "
"are just .kra files which are saved in a special location, so they can be "
"pulled up by Krita quickly. You can make your own template file from any ."
"kra file, by using :menuselection:`File --> Create Template From Image` in "
"the top menu. This will add your current document as a new template, "
"including all its properties along with the layers and layer contents."
msgstr ""
"Estas são categorias separadas onde oferecemos algumas predefinições "
"especiais. Os modelos são apenas ficheiros .kra que são gravados numa "
"localização especial , para que possam ser escolhidos rapidamente pelo "
"Krita. Poderá criar o seu próprio ficheiro de modelo a partir de qualquer "
"ficheiro .kra, usando a opção :menuselection:`Ficheiro --> Criar um Modelo "
"da Imagem` no menu de topo. Isto irá adicionar o seu documento actual como "
"um novo modelo, incluindo todas as suas propriedades em conjunto com as "
"camadas e o conteúdo das mesmas."

#: ../../reference_manual/create_new_document.rst:78
msgid ""
"Once you have created a new document according to your preference, you "
"should now have a white canvas in front of you (or whichever background "
"color you chose in the dialog)."
msgstr ""
"Assim que tiver criado um novo documento de acordo com a sua preferência, "
"deverá agora ter uma área de desenho branca à sua frente (ou com outra cor "
"de fundo qualquer que tiver escolhido na janela)."
