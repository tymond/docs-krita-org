# Translation of docs_krita_org_general_concepts___file_formats___file_gbr.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_gbr\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 09:32+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats/file_gbr.rst:1
msgid "The Gimp Brush file format as used in Krita."
msgstr "Формат файлів пензлів Gimp і як його використовує Krita."

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "Gimp Brush"
msgstr "Пензель GIMP"

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "GBR"
msgstr "GBR"

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "*.gbr"
msgstr "*.gbr"

#: ../../general_concepts/file_formats/file_gbr.rst:15
msgid "\\*.gbr"
msgstr "\\*.gbr"

#: ../../general_concepts/file_formats/file_gbr.rst:17
msgid ""
"The GIMP brush format. Krita can open, save and use these files as :ref:"
"`predefined brushes <predefined_brush_tip>`."
msgstr ""
"Формат файлів пензлів GIMP. Krita може відкривати, зберігати і "
"використовувати файли у цьому форматі як :ref:`попередньо визначені пензлі "
"<predefined_brush_tip>`."

#: ../../general_concepts/file_formats/file_gbr.rst:19
msgid ""
"There's three things that you can decide upon when exporting a ``*.gbr``:"
msgstr ""
"Існує три речі, які слід визначити під час експортування даних до ``*.gbr``:"

#: ../../general_concepts/file_formats/file_gbr.rst:21
msgid "Name"
msgstr "Назва"

#: ../../general_concepts/file_formats/file_gbr.rst:22
msgid ""
"This name is different from the file name, and will be shown inside Krita as "
"the name of the brush."
msgstr ""
"Ця назва відрізняється від назви файла. Її буде показано у Krita як назву "
"пензля."

#: ../../general_concepts/file_formats/file_gbr.rst:23
msgid "Spacing"
msgstr "Інтервал"

#: ../../general_concepts/file_formats/file_gbr.rst:24
msgid "This sets the default spacing."
msgstr "Встановлює типовий інтервал для пензля."

#: ../../general_concepts/file_formats/file_gbr.rst:26
msgid "Use color as mask"
msgstr "Використовувати колір як маску"

#: ../../general_concepts/file_formats/file_gbr.rst:26
msgid ""
"This'll turn the darkest values of the image as the ones that paint, and the "
"whitest as transparent. Untick this if you are using colored images for the "
"brush."
msgstr ""
"Позначення цього пункту зробить найтемніші ділянки зображення видимими, а "
"найсвітліші — прозорими. Зніміть позначку з цього пункту, якщо ви "
"використовуєте для пензля кольорове зображення."

#: ../../general_concepts/file_formats/file_gbr.rst:28
msgid ""
"GBR brushes are otherwise unremarkable, and limited to 8bit color precision."
msgstr ""
"Загалом, пензлі GBR не є ніяким чином видатними. Кольори у них обмежено 8-"
"бітовою точністю на канал."
