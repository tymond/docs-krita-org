# Translation of docs_krita_org_user_manual___selections.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_user_manual___selections\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:30+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../<rst_epilog>:66
msgid ""
".. image:: images/icons/rectangular_select_tool.svg\n"
"   :alt: toolselectrect"
msgstr ""
".. image:: images/icons/rectangular_select_tool.svg\n"
"   :alt: toolselectrect"

#: ../../<rst_epilog>:68
msgid ""
".. image:: images/icons/elliptical_select_tool.svg\n"
"   :alt: toolselectellipse"
msgstr ""
".. image:: images/icons/elliptical_select_tool.svg\n"
"   :alt: toolselectellipse"

#: ../../<rst_epilog>:70
msgid ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: toolselectpolygon"
msgstr ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: toolselectpolygon"

#: ../../<rst_epilog>:72
msgid ""
".. image:: images/icons/path_select_tool.svg\n"
"   :alt: toolselectpath"
msgstr ""
".. image:: images/icons/path_select_tool.svg\n"
"   :alt: toolselectpath"

#: ../../<rst_epilog>:74
msgid ""
".. image:: images/icons/outline_select_tool.svg\n"
"   :alt: toolselectoutline"
msgstr ""
".. image:: images/icons/outline_select_tool.svg\n"
"   :alt: toolselectoutline"

#: ../../<rst_epilog>:76
msgid ""
".. image:: images/icons/contiguous_select_tool.svg\n"
"   :alt: toolselectcontiguous"
msgstr ""
".. image:: images/icons/contiguous_select_tool.svg\n"
"   :alt: toolselectcontiguous"

#: ../../<rst_epilog>:78
msgid ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: toolselectsimilar"
msgstr ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: toolselectsimilar"

#: ../../user_manual/selections.rst:1
msgid "How selections work in Krita."
msgstr "Як працює позначення у Krita."

#: ../../user_manual/selections.rst:1
msgid ":ref:`rectangle_selection_tool`"
msgstr ":ref:`rectangle_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectrect|"
msgstr "|toolselectrect|"

#: ../../user_manual/selections.rst:1
msgid "Select the shape of a square."
msgstr "Вибір у формі квадрата."

#: ../../user_manual/selections.rst:1
msgid ":ref:`ellipse_selection_tool`"
msgstr ":ref:`ellipse_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectellipse|"
msgstr "|toolselectellipse|"

#: ../../user_manual/selections.rst:1
msgid "Select the shape of a circle."
msgstr "Вибір у формі кола."

#: ../../user_manual/selections.rst:1
msgid ":ref:`polygonal_selection_tool`"
msgstr ":ref:`polygonal_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectpolygon|"
msgstr "|toolselectpolygon|"

#: ../../user_manual/selections.rst:1
msgid ""
"Click where you want each point of the Polygon to be. Double click to end "
"your polygon and finalize your selection area. Use the :kbd:`Shift + Z` "
"shortcut to undo last point."
msgstr ""
"Клацайте у точках вершин багатокутника. Двічі клацніть у останній вершині, "
"щоб завершити створення області позначення. Для скасування створення "
"останньої вершини скористайтеся натисканням комбінації клавіш :kbd:`Shift + "
"Z`."

#: ../../user_manual/selections.rst:1
msgid ":ref:`outline_selection_tool`"
msgstr ":ref:`outline_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectoutline|"
msgstr "|toolselectoutline|"

#: ../../user_manual/selections.rst:1
msgid ""
"Outline/Lasso tool is used for a rough selection by drawing the outline."
msgstr ""
"Інструмент контуру або ласо використовується для грубого позначення шляхом "
"малювання контуру."

#: ../../user_manual/selections.rst:1
msgid ":ref:`similar_selection_tool`"
msgstr ":ref:`similar_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectsimilar|"
msgstr "|toolselectsimilar|"

#: ../../user_manual/selections.rst:1
msgid "Similar Color Selection Tool."
msgstr "Інструмент «Вибір подібного кольору»."

#: ../../user_manual/selections.rst:1
msgid ":ref:`contiguous_selection_tool`"
msgstr ":ref:`contiguous_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectcontiguous|"
msgstr "|toolselectcontiguous|"

#: ../../user_manual/selections.rst:1
msgid ""
"Contiguous or “Magic Wand” selects a field of color. Adjust the :guilabel:"
"`Fuzziness` to allow more changes in the field of color, by default limited "
"to the current layer."
msgstr ""
"Неперервна позначення або позначення «Чарівною паличкою» призначено для "
"позначення однаково зафарбованих ділянок. Зміна параметра :guilabel:"
"`Нечіткість` надає змогу змінювати поле кольору. Типово, позначення обмежено "
"поточним шаром."

#: ../../user_manual/selections.rst:1
msgid ":ref:`path_selection_tool`"
msgstr ":ref:`path_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectpath|"
msgstr "|toolselectpath|"

#: ../../user_manual/selections.rst:1
msgid ""
"Path select an area based on a vector path, click to get sharp corners or "
"drag to get flowing lines and close the path with the :kbd:`Enter` key or "
"connecting back to the first point."
msgstr ""
"Позначення контуром призначено для позначення області на основі векторного "
"контуру. Клацніть, щоб отримати гострий кут контуру, перетягуйте, щоб "
"отримати плавні лінії. Замкніть контур натисканням клавіші :kbd:`Enter` або "
"повторним клацанням на початковій точці."

#: ../../user_manual/selections.rst:12
msgid "Selection"
msgstr "Позначення"

#: ../../user_manual/selections.rst:17
msgid "Selections"
msgstr "Позначення"

#: ../../user_manual/selections.rst:19
msgid ""
"Selections allow you to pick a specific area of your artwork to change. This "
"is useful for when you want to move a section, transform it, or paint on it "
"without affecting the other sections. There are many selection tools "
"available that select in different ways. Once an area is selected, most "
"tools will stay inside that area. On that area you can draw or use gradients "
"to quickly get colored and/or shaded shapes with hard edges."
msgstr ""
"За допомогою позначення ви можете вбирати певну область на малюнку для "
"внесення змін. Це корисно, якщо ви хочете пересунути частину, перетворити її "
"або намалювати щось на ній, не змінюючи інших частин. Передбачено багато "
"інструментів позначення для позначення у різний спосіб. Коли область "
"позначено, застосування більшості інструментів стосуватиметься лише пікселів "
"всередині області. У цій області ви можете малювати або використовувати "
"градієнти для швидкого отримання кольорових і/або затінених форм із різкими "
"краями."

#: ../../user_manual/selections.rst:22
msgid "Creating Selections"
msgstr "Створення позначень"

#: ../../user_manual/selections.rst:24
msgid ""
"The most common selection tools all exist at the bottom of the toolbox. Each "
"tool selects things slightly differently. The links for each tool go into a "
"more detailed description of how to use it."
msgstr ""
"Кнопки найпопулярніших інструментів позначення розташовано у нижній частині "
"панелі інструментів. Кожен з інструментів позначає області у власний спосіб. "
"Посилання на пункті кожного з інструментів ведуть на сторінки із докладнішим "
"описом використання кожного з інструментів."

#: ../../user_manual/selections.rst:38
msgid ""
"You can also use the transform tools on your selection, a great way to try "
"different proportions on parts of your image."
msgstr ""
"Крім того, ви можете скористатися для зміни позначеної області інструментами "
"перетворення. Це чудовий спосіб поекспериментувати зі пропорціями частин "
"вашого малюнка."

#: ../../user_manual/selections.rst:41
msgid "Editing Selections"
msgstr "Редагування позначень"

#: ../../user_manual/selections.rst:43
msgid ""
"The tool options for each selection tool gives you the ability to modify "
"your selection."
msgstr ""
"За допомогою параметрів інструмента для кожного із інструментів позначення "
"ви можете змінити параметри вашого позначення."

#: ../../user_manual/selections.rst:47
msgid "Action"
msgstr "Дія"

#: ../../user_manual/selections.rst:47
msgid "Modifier"
msgstr "Модифікатор"

#: ../../user_manual/selections.rst:47
msgid "Shortcut"
msgstr "Скорочення"

#: ../../user_manual/selections.rst:47
msgid "Description"
msgstr "Опис"

#: ../../user_manual/selections.rst:49
msgid "Replace"
msgstr "Замінити"

#: ../../user_manual/selections.rst:49
msgid "Ctrl"
msgstr "Ctrl"

#: ../../user_manual/selections.rst:49
msgid "R"
msgstr "R"

#: ../../user_manual/selections.rst:49
msgid "Replace the current selection."
msgstr "Замінити поточну виділену область."

#: ../../user_manual/selections.rst:51
msgid "Intersect"
msgstr "Перетнути"

#: ../../user_manual/selections.rst:51
msgid "Shift + Alt"
msgstr "Shift + Alt"

#: ../../user_manual/selections.rst:51 ../../user_manual/selections.rst:57
msgid "--"
msgstr "--"

#: ../../user_manual/selections.rst:51
msgid "Get the overlapping section of both selections"
msgstr ""
"Отримати позначення, яке складається із ділянок, де перетинаються обидва "
"позначення."

#: ../../user_manual/selections.rst:53
msgid "Add"
msgstr "Додати"

#: ../../user_manual/selections.rst:53
msgid "Shift"
msgstr "Shift"

#: ../../user_manual/selections.rst:53
msgid "A"
msgstr "A"

#: ../../user_manual/selections.rst:53
msgid "Add the new selection to the current selection."
msgstr "Додати нову позначену ділянку до поточної позначеної ділянки."

#: ../../user_manual/selections.rst:55
msgid "Subtract"
msgstr "Відняти"

#: ../../user_manual/selections.rst:55
msgid "Alt"
msgstr "Alt"

#: ../../user_manual/selections.rst:55
msgid "S"
msgstr "S"

#: ../../user_manual/selections.rst:55
msgid "Subtract the selection from the current selection."
msgstr "Відняти позначене від поточної позначеної ділянки."

#: ../../user_manual/selections.rst:57
msgid "Symmetric Difference"
msgstr "Симетрична різниця"

#: ../../user_manual/selections.rst:57
msgid "Make a selection where both the new and current do not overlap."
msgstr ""
"Створити позначену ділянку, яка складатиметься з ділянок, де не "
"перетинаються нова і поточна позначена ділянки."

#: ../../user_manual/selections.rst:61
msgid "You can change this in :ref:`tool_options_settings`."
msgstr "Змінити це можна за допомогою сторінки :ref:`tool_options_settings`."

#: ../../user_manual/selections.rst:63
msgid ""
"If you hover over a selection with a selection tool and no selection is "
"activated, you can move it. To quickly go into transform mode, |mouseright| "
"and select :guilabel:`Edit Selection`."
msgstr ""
"Якщо ви наведете вказівник на область зображення, коли буде активовано "
"інструмент позначення, а жодну із областей позначення не буде активовано, ви "
"зможете пересунути область позначення. Щоб швидко перейти до режиму "
"перетворення, клацніть |mouseright| і виберіть у контекстному меню пункт :"
"guilabel:`Змінити позначене`."

#: ../../user_manual/selections.rst:66
msgid "Removing Selections"
msgstr "Вилучення позначень"

#: ../../user_manual/selections.rst:68
msgid ""
"If you want to delete the entire selection, the easiest way is to deselect "
"everything. :menuselection:`Select --> Deselect`. Shortcut :kbd:`Ctrl + "
"Shift + A`."
msgstr ""
"Якщо ви хочете скасувати усі позначення, найпростішим буде скористатися "
"пунктом скасовування позначень — :menuselection:`Вибір --> Скасувати "
"позначення`. Клавіатурне скорочення — :kbd:`Ctrl + Shift + A`."

#: ../../user_manual/selections.rst:71
msgid "Display Modes"
msgstr "Режими показу"

#: ../../user_manual/selections.rst:73
msgid ""
"In the bottom left-hand corner of the status bar there is a button to toggle "
"how the selection is displayed. The two display modes are the following: "
"(Marching) Ants and Mask. The red color with Mask can be changed in the "
"preferences. You can edit the color under :menuselection:`Settings --> "
"Configure Krita --> Display --> Selection Overlay`. If there is no "
"selection, this button will not do anything."
msgstr ""
"У нижньому лівому куті смужки стану вікна програми є кнопка для перемикання "
"способу показу позначеної області. Передбачено два режими показу: (рухливі) "
"мурахи і маска. Червоний колір для режиму маски можна змінити у "
"налаштуваннях програми. Для цього слід скористатися сторінкою :menuselection:"
"`Параметри --> Налаштувати Krita --> Монітор --> Накладка позначення`. Якщо "
"на полотні нічого не позначено, перемикання кнопки нічого не змінюватиме."

#: ../../user_manual/selections.rst:77
msgid ".. image:: images/selection/Ants-displayMode.jpg"
msgstr ".. image:: images/selection/Ants-displayMode.jpg"

#: ../../user_manual/selections.rst:78
msgid ""
"Ants display mode (default) is best if you want to see the areas that are "
"not selected."
msgstr ""
"Режим показу «Мурахи» (типовий) є найкращим варіантом, якщо ви хочете бачити "
"області, які не позначено."

#: ../../user_manual/selections.rst:81
msgid ".. image:: images/selection/Mask-displayMode.jpg"
msgstr ".. image:: images/selection/Mask-displayMode.jpg"

#: ../../user_manual/selections.rst:82
msgid ""
"Mask display mode is good if you are interested in seeing the various "
"transparency levels for your selection. For example, when you have a "
"selection with very soft edges due using feathering."
msgstr ""
"Режим показу маски може бути використано, якщо ви хочете бачити різноманітні "
"рівні прозорості для позначеної вами області. Наприклад, на вашому малюнку "
"може бути позначений фрагмент із дуже м'якими краями через використання "
"розмазування."

#: ../../user_manual/selections.rst:86
msgid ""
"Mask mode is activated as well when a selection mask is the active layer so "
"you can see the different selection levels."
msgstr ""
"Якщо активним шаром є маска позначення, буде активовано режим маскування, "
"отже ви зможете бачити різні рівні позначення."

#: ../../user_manual/selections.rst:89
msgid "Global Selection Mask (Painting a Selection)"
msgstr "Маска загального вибору (малювання позначеної області)"

#: ../../user_manual/selections.rst:91
msgid ""
"The global Selection Mask is your selection that appears on the layers "
"docker. By default, this is hidden, so you will need to make it visible via :"
"menuselection:`Select --> Show Global Selection Mask`."
msgstr ""
"Загальна маска позначення — позначена вами область, яку показано на бічній "
"панелі шарів. Типово, її пункт приховано, тому вам слід зробити його видимим "
"за допомогою пункту меню :menuselection:`Вибір --> Показати маску загального "
"вибору`."

#: ../../user_manual/selections.rst:94
msgid ".. image:: images/selection/Global-selection-mask.jpg"
msgstr ".. image:: images/selection/Global-selection-mask.jpg"

#: ../../user_manual/selections.rst:95
msgid ""
"Once the global Selection Mask is shown, you will need to create a "
"selection. The benefit of using this is that you can paint your selection "
"using any of the normal painting tools, including the transform and move. "
"The information is saved as grayscale."
msgstr ""
"Щойно буде увімкнено показ загальної маски позначення, вам варто буде "
"створити позначення. Перевагою використання цього режиму є те, що ви зможете "
"малювати вашу позначену область за допомогою будь-яких звичайних "
"інструментів малювання, зокрема і перетворення та пересування. Дані "
"зберігатимуться у відтінках сірого кольору."

#: ../../user_manual/selections.rst:98
msgid ""
"You can enter the global selection mask mode quickly from the selection "
"tools by doing |mouseright| and select :guilabel:`Edit Selection`."
msgstr ""
"Ви можете швидко увімкнути режим маски загального позначення за допомогою "
"панелі інструментів позначення: клацніть |mouseright| і виберіть :guilabel:"
"`Змінити позначене`."

#: ../../user_manual/selections.rst:101
msgid "Selection from layer transparency"
msgstr "Позначення з прозорості шару"

#: ../../user_manual/selections.rst:104
msgid ""
"You can create a selection based on a layer's transparency by right-clicking "
"on the layer in the layer docker and selecting :guilabel:`Select Opaque` "
"from the context menu."
msgstr ""
"Ви можете створити позначену область на основі даних щодо прозорості шару. "
"Клацніть правою кнопкою на пункті шару на бічній панелі списку шарів і "
"виберіть у контекстному меню пункт :guilabel:`Вибрати непрозорість`."

#: ../../user_manual/selections.rst:108
msgid ""
"You can also do this for adding, subtracting and intersecting by going to :"
"menuselection:`Select --> Select Opaque`, where you can find specific "
"actions for each."
msgstr ""
"Крім того, можна створити позначену область додаванням, відніманням або "
"перетинанням областей за допомогою пункту меню :menuselection:`Вибір --> "
"Вибрати непрозорість`. У відповідному підменю ви знайдете усі дії щодо "
"логічних операцій із позначеними областями."

#: ../../user_manual/selections.rst:110
msgid ""
"If you want to quickly select parts of layers, you can hold the :kbd:`Ctrl "
"+` |mouseleft| shortcut on the layer *thumbnail*. To add a selection do :kbd:"
"`Ctrl + Shift +` |mouseleft|, to remove :kbd:`Ctrl + Alt +` |mouseleft| and "
"to intersect :kbd:`Ctrl + Shift + Alt +` |mouseleft|. This works with any "
"mask that has pixel or vector data (so everything but transform masks)."
msgstr ""
"Якщо вам потрібно швидко позначити частини шарів, ви можете натиснути і "
"утримувати комбінацію :kbd:`Ctrl +` |mouseleft| на *мініатюрі* шару. Щоб "
"додати позначене скористайтеся комбінацією :kbd:`Ctrl + Shift +` |"
"mouseleft|; щоб вилучити — :kbd:`Ctrl + Alt +` |mouseleft|; а щоб перетнути "
"— :kbd:`Ctrl + Shift + Alt` |mouseleft|. Ці комбінації працюють із будь-якою "
"маскою, яка містить піксельні або векторні дані (отже, з усім, окрім масок "
"перетворення)."

#: ../../user_manual/selections.rst:114
msgid "Pixel and Vector Selection Types"
msgstr "Типи піксельного і векторного позначення"

#: ../../user_manual/selections.rst:116
msgid ""
"Vector selections allow you to modify your selection with vector anchor "
"tools. Pixel selections allow you to modify selections with pixel "
"information. They both have their benefits and disadvantages. You can "
"convert one type of selection to another."
msgstr ""
"Використання векторних областей позначення надає вам змогу змінювати контури "
"позначеної області за допомогою точок прив'язки векторних дуг. Використання "
"піксельних позначень уможливлює внесення змін до контуру позначеної області "
"на основі даних пікселів малюнка. У кожного з варіантів є власні переваги і "
"недоліки. Передбачено можливість перетворення позначеної області одного типу "
"на позначену область іншого типу."

#: ../../user_manual/selections.rst:119
msgid ".. image:: images/selection/Vector-pixel-selections.jpg"
msgstr ".. image:: images/selection/Vector-pixel-selections.jpg"

#: ../../user_manual/selections.rst:120
msgid ""
"When creating a selection, you can select what type of selection you want "
"from the Mode in the selection tool options: Pixel or Vector. By default "
"this will be a vector."
msgstr ""
"При створенні позначення ви можете вибрати потрібний вам тип позначення у "
"полі :guilabel:`Режим` параметрів інструмента позначення: «Піксельний» або "
"«Векторний». Типовим буде векторний режим."

#: ../../user_manual/selections.rst:122
msgid ""
"Vector selections can be modified as any other :ref:`vector shape "
"<vector_graphics>` with the :ref:`shape_selection_tool`, if you try to paint "
"on a vector selection mask it will be converted into a pixel selection. You "
"can also convert vector shapes to selection. In turn, vector selections can "
"be made from vector shapes, and vector shapes can be converted to vector "
"selections using the options in the :guilabel:`Selection` menu. Krita will "
"add a new vector layer for this shape."
msgstr ""
"Змінювати векторні позначення можна, які і будь-яку :ref:`векторну форму "
"<vector_graphics>`, за допомогою :ref:`інструмента позначення форм "
"<shape_selection_tool>`. Якщо ви спробуєте щось намалювати на позначеній "
"векторній області, її буде перетворено на піксельну. Передбачено можливість "
"перетворення векторних форм та області позначення. І навпаки, векторні "
"області позначення можна створювати на основі векторних форм. Векторні форми "
"можна перетворити на векторні позначення за допомогою відповідних пунктів "
"меню. Krita автоматично додасть новий векторний шар для перетвореної форми."

#: ../../user_manual/selections.rst:124
msgid ""
"One of the most common reasons to use vector selections is that they give "
"you the ability to move and transform a selection without the kind of resize "
"artifacts you get with a pixel selection. You can also use the :ref:"
"`shape_edit_tool` to change the anchor points in the selection, allowing you "
"to precisely adjust bezier curves or add corners to rectangular selections."
msgstr ""
"Однією зі типових причин використання векторних позначень є те, що за їхньою "
"допомогою можна отримувати позначені області, які можна пересувати і "
"перетворювати без втрати якості, яка спостерігається при перетворенні або "
"пересуванні піксельних позначених областей. Крім того, ви можете "
"скористатися :ref:`інструментом редагування контурів <shape_edit_tool>` для "
"зміни точок прив'язки (керівних точок) позначеної області. Цей інструмент "
"надає змогу точно скоригувати криві Безьє та додати кути до прямокутних "
"областей позначення."

#: ../../user_manual/selections.rst:126
msgid ""
"If you started with a pixel selection, you can still convert it to a vector "
"selection to get these benefits. Go to :menuselection:`Select --> Convert to "
"Vector Selection`."
msgstr ""
"Якщо ви спочатку створити піксельне позначення, ви можете перетворити його "
"на векторне, щоб скористатися усіма описаними вище перевагами. Для цього "
"скористайтеся пунктом меню :menuselection:`Вибір --> Перетворити на "
"векторний вибір`."

#: ../../user_manual/selections.rst:130
msgid ""
"If you have multiple levels of transparency when you convert a selection to "
"vector, you will lose the semi-transparent values."
msgstr ""
"Якщо ви визначили декілька рівнів прозорості, під час перетворення "
"позначеного у векторний шар ви втратите усі значення напівпрозорості для "
"перетворених даних."

#: ../../user_manual/selections.rst:133
msgid "Common Shortcuts while Using Selections"
msgstr "Типові клавіатурні скорочення при роботі з позначеними областями"

#: ../../user_manual/selections.rst:135
msgid "Copy -- :kbd:`Ctrl + C` or :kbd:`Ctrl + Ins`"
msgstr "Копіювати — :kbd:`Ctrl + C` або :kbd:`Ctrl + Ins`"

#: ../../user_manual/selections.rst:136
msgid "Paste -- :kbd:`Ctrl + V` or :kbd:`Shift + Ins`"
msgstr "Вставити — :kbd:`Ctrl + V` або :kbd:`Shift + Ins`"

#: ../../user_manual/selections.rst:137
msgid "Cut -- :kbd:`Ctrl + X`, :kbd:`Shift + Del`"
msgstr "Вирізати — :kbd:`Ctrl + X`, :kbd:`Shift + Del`"

#: ../../user_manual/selections.rst:138
msgid "Copy From All Layers -- :kbd:`Ctrl + Shift + C`"
msgstr "Копіювати з усіх шарів — :kbd:`Ctrl + Shift + C`"

#: ../../user_manual/selections.rst:139
msgid "Copy Selection to New Layer -- :kbd:`Ctrl + Alt + J`"
msgstr "Копіювати позначене до нового шару — :kbd:`Ctrl + Alt + J`"

#: ../../user_manual/selections.rst:140
msgid "Cut Selection to New Layer -- :kbd:`Ctrl + Shift + J`"
msgstr "Вирізати позначене до нового шару — :kbd:`Ctrl + Shift + J`"

#: ../../user_manual/selections.rst:141
msgid "Display or hide selection with :kbd:`Ctrl + H`"
msgstr "Показати або приховати позначене за допомогою :kbd:`Ctrl + H`"

#: ../../user_manual/selections.rst:142
msgid "Select Opaque -- :kbd:`Ctrl +` |mouseleft| on layer thumbnail."
msgstr "Вибрати непрозорість — :kbd:`Ctrl +` |mouseleft| на мініатюрі шару."

#: ../../user_manual/selections.rst:143
msgid ""
"Select Opaque (Add) -- :kbd:`Ctrl + Shift +` |mouseleft| on layer thumbnail."
msgstr ""
"Вибрати непрозорість (Додати) — :kbd:`Ctrl + Shift +` |mouseleft| на "
"мініатюрі шару."

#: ../../user_manual/selections.rst:144
msgid ""
"Select Opaque (Subtract) -- :kbd:`Ctrl + Alt +` |mouseleft| on layer "
"thumbnail."
msgstr ""
"Вибрати непрозорість (Відняти) — :kbd:`Ctrl + Alt +` |mouseleft| на "
"мініатюрі шару."

#: ../../user_manual/selections.rst:145
msgid ""
"Select Opaque (Intersect) -- :kbd:`Ctrl + Shift + Alt +` |mouseleft| on "
"layer thumbnail."
msgstr ""
"Вибрати непрозорість (Перетнути) — :kbd:`Ctrl + Shift + Alt +` |mouseleft| "
"на мініатюрі шару."
