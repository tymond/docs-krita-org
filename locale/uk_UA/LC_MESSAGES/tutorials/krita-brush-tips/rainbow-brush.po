# Translation of docs_krita_org_tutorials___krita-brush-tips___rainbow-brush.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_tutorials___krita-brush-tips___rainbow-"
"brush\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:50+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:None
msgid ""
".. image:: images/brush-tips/Brushtip-Rainbow.png\n"
"   :alt: selecting fill circle for brush tip"
msgstr ""
".. image:: images/brush-tips/Brushtip-Rainbow.png\n"
"   :alt: Вибір заповненого кола для кінчика пензля"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:None
msgid ""
".. image:: images/brush-tips/Brushtip-Rainbow_2.png\n"
"   :alt: toggle hue in the brush parameter"
msgstr ""
".. image:: images/brush-tips/Brushtip-Rainbow_2.png\n"
"   :alt: Перемикання на сторінку відтінку у параметрах пензля"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:None
msgid ""
".. image:: images/brush-tips/Brushtip-Rainbow_3.png\n"
"   :alt: select distance parameter for the hue"
msgstr ""
".. image:: images/brush-tips/Brushtip-Rainbow_3.png\n"
"   :alt: Виберіть параметр відстані для відтінку"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:1
msgid "A tutorial about making rainbow brush in krita"
msgstr "Підручник зі створення райдужного пензля у krita"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:13
msgid "Brush-tips:Rainbow Brush"
msgstr "Кінчики пензлів: Райдужний пензель"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:16
msgid "Question"
msgstr "Питання"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:18
msgid "**Hello, there is a way to paint with rainbow on Krita?**"
msgstr ""
"**Привіт, можна якось зробити так, щоб у Krita можна було малювати веселкою?"
"**"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:20
msgid "Yes there is."
msgstr "Так, можна."

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:22
msgid "First, select the fill_circle:"
msgstr "Спочатку, виберіть fill_circle:"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:27
msgid ""
"Then, press the :kbd:`F5` key to open the brush editor, and toggle **Hue**."
msgstr ""
"Далі, натисніть :kbd:`F5`, щоб відкрити вікно редактора пензлів, і "
"перемкніться на **Відтінок**."

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:32
msgid "This should allow you to change the color depending on the pressure."
msgstr "Тут ви зможете змінити параметри вибору кольору залежно від тиску."

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:34
msgid ""
"The brightness of the rainbow is relative to the color of the currently "
"selected color, so make sure to select bright saturated colors for a bright "
"rainbow!"
msgstr ""
"Яскравість веселки залежатиме від поточного вибраного кольору, тому "
"вибирайте яскраві насичені кольори, щоб отримати яскраву веселку!"

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:36
msgid ""
"Uncheck **Pressure** and check **Distance** to make the rainbow paint itself "
"over distance. The slider below can be |mouseright| to change the value with "
"keyboard input."
msgstr ""
"Зніміть позначку з пункту **Тиск** і позначте пункт **Відстань**, щоб "
"автоматично малювати веселку за відстанню. На повзунку, який розташовано "
"нижче, можна клацнути |mouseright|, щоб значення змінювалося за допомогою "
"клавіатури."

#: ../../tutorials/krita-brush-tips/rainbow-brush.rst:41
msgid "When you are satisfied, give the brush a new name and save it."
msgstr ""
"Коли налаштовування буде завершено, надайте пензлю нової назви і збережіть "
"його."
